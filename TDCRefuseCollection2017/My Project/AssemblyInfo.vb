﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes
<Assembly: AssemblyTitle("wsTDCRefuseCollection2017")>
<Assembly: AssemblyDescription("Web service for supplying refuse collection data")>
<Assembly: AssemblyCompany("Tandridge DC")>
<Assembly: AssemblyProduct("TDCRefuseCollection2017")>
<Assembly: AssemblyCopyright("Copyright © Tandridge DC 2017")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("4b89842c-868b-4e06-b6b0-fd2e20f0cfc2")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")>
<Assembly: AssemblyFileVersion("1.0.0.0")>
