﻿Imports System.Xml.Serialization
Imports TDCRefuseCollection2017

<XmlRoot(Namespace:="http://tandridge.gov.uk/TDCRefuseCollection2017/", ElementName:="RefuseCollectionDetails", IsNullable:=True)>
Public Class clsRefuseCollectionRecord
	Implements IComparable(Of clsRefuseCollectionRecord)

	<XmlAttribute>
	Public SearchUPRN As String = ""
	<XmlAttribute>
	Public SearchDate As Date
	<XmlAttribute>
	Public UPRNRecordFound As Boolean = False
	<XmlAttribute>
	Public CollectionRecordFound As Boolean = False

	<XmlElement("RefuseCollectionSchedule")>
	Public BiffaSchedule As New clsBiffaSchedules
	Public AddressRecord As wsNLPGSearch.AddressBaseRecord

	Public Sub New()
		Try

			SearchDate = Now

		Catch ex As Exception
			Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
		End Try
	End Sub

	Public Sub New(URPN As String)
		Try

			SearchDate = Now
			SearchUPRN = SearchUPRN

			LoadDetails()

		Catch ex As Exception
			Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
		End Try
	End Sub


	Public Sub New(UPRN As String, DateToSearch As Date)
		Try

			If DateToSearch.Ticks <> 0 Then SearchDate = DateToSearch Else SearchDate = Now.Date
			SearchUPRN = UPRN

			LoadDetails()

		Catch ex As Exception
			Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
		End Try
	End Sub


	Protected Friend Sub LoadDetails()
		Try

			LoadRecordOfUPRN(SearchUPRN)
			If UPRNRecordFound Then
				GetRefuseDetailsSQL(SearchUPRN, SearchDate)
				BiffaSchedule.NextHolidays.Sort()
			End If

		Catch ex As Exception
			Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
		End Try

	End Sub



	Public Sub LoadRecordOfUPRN(sUPRN As String)
		Dim oResponse As wsNLPGSearch.AddressBaseRecord, oWS As New wsNLPGSearch.QueryAddressBase2016
		Try

			oResponse = oWS.GetUPRNAddress(sUPRN)
			If oResponse IsNot Nothing Then AddressRecord = oResponse
			UPRNRecordFound = (oResponse.UPRN <> "")

		Catch ex As Exception
			Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)
		End Try
	End Sub



	Public Sub GetRefuseDetailsODBC(Optional SearchUPRNin As String = "", Optional SearchDateIn As Date = Nothing)
		Dim sSQL As String, sODBCConnectString As String
		Dim oODBCConn As Odbc.OdbcConnection, oODBCCommand As Odbc.OdbcCommand
		Dim oReader As Odbc.OdbcDataReader, sTemp As String = "", sKey As String = ""
		Dim dTest As Date

		Try
			If SearchUPRNin = "" Then SearchUPRNin = SearchUPRN
			If SearchDateIn = Nothing Then SearchDateIn = SearchDate
			If SearchDateIn = Nothing Then SearchDateIn = Now.Date

			sODBCConnectString = GetMySettingString("RefuseDBODBCConnectString")
			If sODBCConnectString <> "" And SearchUPRNin <> "" Then
				sSQL = GetMySettingString("RefuseDBODBCSQLSelection")
				sSQL = String.Format(sSQL, MakeStringSQLValueSafe(SearchUPRNin))
				sSQL = String.Format(sSQL, MakeStringSQLValueSafe(SearchDateIn.ToString("s").Replace("T", " ")))

				oODBCConn = New Odbc.OdbcConnection(sODBCConnectString)
				oODBCConn.Open()
				If oODBCConn.State = ConnectionState.Open Then
					oODBCCommand = New Odbc.OdbcCommand(sSQL, oODBCConn)

					oReader = oODBCCommand.ExecuteReader
					If oReader.HasRows Then
						oReader.Read()
						BiffaSchedule.DateToCheck = SearchDateIn
						CollectionRecordFound = True

						sKey = "AWC_Calendar" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.AWC_Calendar = sTemp ' Date.Parse(sTemp)
						sKey = "AWC_Schedule" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.AWC_Schedule = sTemp
						sKey = "Road_Hazard" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.RoadHazard = sTemp
						sKey = "Communal_Site_ID" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.CommunalSiteID = sTemp

						sKey = "Dw_Day" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.DomesticWaste.Day = sTemp
						sKey = "Dw_Order" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.DomesticWaste.Order = CInt(Math.Floor(Val(sTemp)))
						sKey = "Dw_Crew" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.DomesticWaste.Crew = sTemp
						sKey = "Dw_Bin_Type" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.DomesticWaste.BinType = sTemp
						sKey = "Dw_Next_Collection" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.DomesticWaste.StartDate = Date.Parse(sTemp)
						BiffaSchedule.DomesticWaste.CollectionSchedule = BiffaSchedule.AWC_Schedule
						dTest = BiffaSchedule.DomesticWaste.NextActualCollectionDate


						sKey = "Ry_Day" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.RecyclingWaste.Day = sTemp
						sKey = "Ry_Order" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.RecyclingWaste.Order = CInt(Math.Floor(Val(sTemp)))
						sKey = "Ry_Crew" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.RecyclingWaste.Crew = sTemp
						sKey = "Ry_Bin_Type" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.RecyclingWaste.BinType = sTemp
						sKey = "Ry_Next_Collection" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.RecyclingWaste.StartDate = Date.Parse(sTemp)
						BiffaSchedule.RecyclingWaste.CollectionSchedule = BiffaSchedule.AWC_Schedule
						dTest = BiffaSchedule.RecyclingWaste.NextActualCollectionDate

						sKey = "Fw_Day" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.FoodWaste.Day = sTemp
						'sKey = "Fw_Order" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.FoodWaste.Order = CInt(Math.Floor(Val(sTemp)))
						sKey = "Fw_Crew" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.FoodWaste.Crew = sTemp
						sKey = "Fw_Bin_Type" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.FoodWaste.BinType = sTemp
						sKey = "Fw_Next_Collection" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.FoodWaste.StartDate = Date.Parse(sTemp)
						If BiffaSchedule.FoodWaste.StartDate.Ticks = 0 Then BiffaSchedule.FoodWaste.StartDate = CDate(IIf(BiffaSchedule.DomesticWaste.NextActualCollectionDate.CompareTo(BiffaSchedule.RecyclingWaste.NextActualCollectionDate) > 0, BiffaSchedule.RecyclingWaste.NextActualCollectionDate, BiffaSchedule.DomesticWaste.NextActualCollectionDate))
						BiffaSchedule.FoodWaste.CollectionSchedule = "WEEKLY"
						dTest = BiffaSchedule.FoodWaste.NextActualCollectionDate


						sKey = "Gw_Day" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.GardenWaste.Day = sTemp
						sKey = "Gw_Order" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.GardenWaste.Order = CInt(Math.Floor(Val(sTemp)))
						sKey = "Gw_Crew" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.GardenWaste.Crew = sTemp
						'sKey = "Gw_Bin_Type" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.GardenWaste.BinType = sTemp
						sKey = "Gw_Next_Collection" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.GardenWaste.StartDate = Date.Parse(sTemp)
						If BiffaSchedule.GardenWaste.StartDate.Ticks = 0 Then BiffaSchedule.GardenWaste.StartDate = CDate(IIf(BiffaSchedule.DomesticWaste.NextActualCollectionDate.CompareTo(BiffaSchedule.RecyclingWaste.NextActualCollectionDate) > 0, BiffaSchedule.RecyclingWaste.NextActualCollectionDate, BiffaSchedule.DomesticWaste.NextActualCollectionDate))
						BiffaSchedule.FoodWaste.CollectionSchedule = "WEEKLY"
						dTest = BiffaSchedule.GardenWaste.NextActualCollectionDate

					End If

				End If

			End If

		Catch ex As Exception
			Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

		End Try
		If oReader IsNot Nothing Then Try : oReader.Close() : Catch ex As Exception : End Try
		oReader = Nothing
		oODBCCommand = Nothing
		If oODBCConn IsNot Nothing Then Try : oODBCConn.Close() : Catch ex As Exception : End Try
		oODBCConn = Nothing

	End Sub



	Public Sub GetRefuseDetailsOleDB(Optional SearchUPRNin As String = "", Optional SearchDateIn As Date = Nothing)
		Dim sSQL As String, sOleDBConnectString As String
		Dim oOleDBConn As OleDb.OleDbConnection, oOleDBCommand As OleDb.OleDbCommand
		Dim oReader As OleDb.OleDbDataReader, sTemp As String = "", sKey As String = ""

		Try
			If SearchUPRNin = "" Then SearchUPRNin = SearchUPRN
			If SearchDateIn = Nothing Then SearchDateIn = SearchDate
			If SearchDateIn = Nothing Then SearchDateIn = Now.Date

			sOleDBConnectString = GetMySettingString("RefuseDBOleDBConnectString")
			If sOleDBConnectString <> "" And SearchUPRNin <> "" Then
				sSQL = GetMySettingString("RefuseDBOleDBSQLSelection")
				sSQL = String.Format(sSQL, MakeStringSQLValueSafe(SearchUPRNin))
				sSQL = String.Format(sSQL, MakeStringSQLValueSafe(SearchDateIn.ToString("s").Replace("T", " ")))

				oOleDBConn = New OleDb.OleDbConnection(sOleDBConnectString)
				oOleDBConn.Open()
				If oOleDBConn.State = ConnectionState.Open Then
					oOleDBCommand = New OleDb.OleDbCommand(sSQL, oOleDBConn)

					oReader = oOleDBCommand.ExecuteReader
					If oReader.HasRows Then
						oReader.Read()
						BiffaSchedule.DateToCheck = SearchDateIn
						CollectionRecordFound = True

						sKey = "AWC_Calendar" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.AWC_Calendar = sTemp ' Date.Parse(sTemp)
						sKey = "AWC_Schedule" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.AWC_Schedule = sTemp
						sKey = "Road_Hazard" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.RoadHazard = sTemp
						sKey = "Communal_Site_ID" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.CommunalSiteID = sTemp

						sKey = "Dw_Day" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.DomesticWaste.Day = sTemp
						sKey = "Dw_Order" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.DomesticWaste.Order = CInt(Math.Floor(Val(sTemp)))
						sKey = "Dw_Crew" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.DomesticWaste.Crew = sTemp
						sKey = "Dw_Bin_Type" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.DomesticWaste.BinType = sTemp
						sKey = "Dw_Next_Collection" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.DomesticWaste.StartDate = Date.Parse(sTemp)
						BiffaSchedule.DomesticWaste.CollectionSchedule = BiffaSchedule.AWC_Schedule


						sKey = "Ry_Day" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.RecyclingWaste.Day = sTemp
						sKey = "Ry_Order" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.RecyclingWaste.Order = CInt(Math.Floor(Val(sTemp)))
						sKey = "Ry_Crew" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.RecyclingWaste.Crew = sTemp
						sKey = "Ry_Bin_Type" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.RecyclingWaste.BinType = sTemp
						sKey = "Ry_Next_Collection" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.RecyclingWaste.StartDate = Date.Parse(sTemp)
						BiffaSchedule.RecyclingWaste.CollectionSchedule = BiffaSchedule.AWC_Schedule

						sKey = "Fw_Day" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.FoodWaste.Day = sTemp
						'sKey = "Fw_Order" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.FoodWaste.Order = CInt(Math.Floor(Val(sTemp)))
						sKey = "Fw_Crew" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.FoodWaste.Crew = sTemp
						sKey = "Fw_Bin_Type" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.FoodWaste.BinType = sTemp
						sKey = "Fw_Next_Collection" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.FoodWaste.StartDate = Date.Parse(sTemp)
						If BiffaSchedule.FoodWaste.StartDate.Ticks = 0 Then BiffaSchedule.FoodWaste.StartDate = CDate(IIf(BiffaSchedule.DomesticWaste.NextActualCollectionDate.CompareTo(BiffaSchedule.RecyclingWaste.NextActualCollectionDate) > 0, BiffaSchedule.RecyclingWaste.NextActualCollectionDate, BiffaSchedule.DomesticWaste.NextActualCollectionDate))
						BiffaSchedule.FoodWaste.CollectionSchedule = "WEEKLY"


						sKey = "Gw_Day" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.GardenWaste.Day = sTemp
						sKey = "Gw_Order" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.GardenWaste.Order = CInt(Math.Floor(Val(sTemp)))
						sKey = "Gw_Crew" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.GardenWaste.Crew = sTemp
						'sKey = "Gw_Bin_Type" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.GardenWaste.BinType = sTemp
						sKey = "Gw_Next_Collection" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.GardenWaste.StartDate = Date.Parse(sTemp)
						If BiffaSchedule.GardenWaste.StartDate.Ticks = 0 Then BiffaSchedule.GardenWaste.StartDate = CDate(IIf(BiffaSchedule.DomesticWaste.NextActualCollectionDate.CompareTo(BiffaSchedule.RecyclingWaste.NextActualCollectionDate) > 0, BiffaSchedule.RecyclingWaste.NextActualCollectionDate, BiffaSchedule.DomesticWaste.NextActualCollectionDate))
						BiffaSchedule.FoodWaste.CollectionSchedule = "WEEKLY"

					End If

				End If

			End If

		Catch ex As Exception
			Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

		End Try
		If oReader IsNot Nothing Then Try : oReader.Close() : Catch ex As Exception : End Try
		oReader = Nothing
		oOleDBCommand = Nothing
		If oOleDBConn IsNot Nothing Then Try : oOleDBConn.Close() : Catch ex As Exception : End Try
		oOleDBConn = Nothing
	End Sub




	Public Sub GetRefuseDetailsSQL(Optional SearchUPRNin As String = "", Optional SearchDateIn As Date = Nothing)
		Dim sSQL As String, sSQLConnectString As String
		Dim oSQLConn As SqlClient.SqlConnection, oSQLCommand As SqlClient.SqlCommand
		Dim oReader As SqlClient.SqlDataReader, sTemp As String = "", sKey As String = ""
		Dim dTemp As Date

		Try
			If SearchUPRNin = "" Then SearchUPRNin = SearchUPRN
			If SearchDateIn = Nothing Then SearchDateIn = SearchDate
			If SearchDateIn = Nothing Then SearchDateIn = Now.Date
			BiffaSchedule.DateToCheck = SearchDateIn

			sSQLConnectString = GetMySettingString("RefuseDBMSSQLSConnectString")
			If sSQLConnectString <> "" And SearchUPRNin <> "" Then
				sSQL = GetMySettingString("RefuseDBMSSQLSelection")
				sSQL = String.Format(sSQL, MakeStringSQLValueSafe(SearchUPRNin))
				sSQL = String.Format(sSQL, MakeStringSQLValueSafe(SearchDateIn.ToString("s").Replace("T", " ")))

				oSQLConn = New SqlClient.SqlConnection(sSQLConnectString)
				oSQLConn.Open()
				If oSQLConn.State = ConnectionState.Open Then
					oSQLCommand = New SqlClient.SqlCommand(sSQL, oSQLConn)

					oReader = oSQLCommand.ExecuteReader
					If oReader.HasRows Then
						oReader.Read()
						CollectionRecordFound = True

						sKey = "AWC_Calendar" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.AWC_Calendar = sTemp ' Date.Parse(sTemp)
						sKey = "AWC_Schedule" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.AWC_Schedule = sTemp
						sKey = "Road_Hazard" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.RoadHazard = sTemp
						sKey = "Communal_Site_ID" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.CommunalSiteID = sTemp

						BiffaSchedule.DomesticWaste.RefuseType = "Domestic"
						sKey = "Dw_Day" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.DomesticWaste.Day = sTemp : BiffaSchedule.NormalCollectionDay = sTemp

						sKey = "Dw_Order" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.DomesticWaste.Order = CInt(Math.Floor(Val(sTemp)))
						sKey = "Dw_Crew" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.DomesticWaste.Crew = sTemp
						sKey = "Dw_Bin_Type" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.DomesticWaste.BinType = sTemp
						sKey = "Dw_Next_Collection" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.DomesticWaste.CollectionSchedule = BiffaSchedule.AWC_Schedule
						BiffaSchedule.DomesticWaste.StartDate = Date.Parse(sTemp)
						dTemp = BiffaSchedule.DomesticWaste.NextActualCollectionDate


						BiffaSchedule.RecyclingWaste.RefuseType = "Recycling"
						sKey = "Ry_Day" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.RecyclingWaste.Day = sTemp
						sKey = "Ry_Order" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.RecyclingWaste.Order = CInt(Math.Floor(Val(sTemp)))
						sKey = "Ry_Crew" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.RecyclingWaste.Crew = sTemp
						sKey = "Ry_Bin_Type" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.RecyclingWaste.BinType = sTemp
						sKey = "Ry_Next_Collection" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.RecyclingWaste.CollectionSchedule = BiffaSchedule.AWC_Schedule
						BiffaSchedule.RecyclingWaste.StartDate = Date.Parse(sTemp)
						dTemp = BiffaSchedule.RecyclingWaste.NextActualCollectionDate


						BiffaSchedule.FoodWaste.RefuseType = "Food"
						sKey = "Fw_Day" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.FoodWaste.Day = sTemp
						'sKey = "Fw_Order" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.FoodWaste.Order = CInt(Math.Floor(Val(sTemp)))
						sKey = "Fw_Crew" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.FoodWaste.Crew = sTemp
						sKey = "Fw_Bin_Type" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.FoodWaste.BinType = sTemp
						sKey = "Fw_Next_Collection" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.FoodWaste.StartDate = Date.Parse(sTemp)
						BiffaSchedule.FoodWaste.CollectionSchedule = "WEEKLY"

						If BiffaSchedule.FoodWaste.StartDate.Ticks = 0 Then BiffaSchedule.FoodWaste.StartDate = CDate(IIf(BiffaSchedule.DomesticWaste.NextIntendedCollectionDate.CompareTo(BiffaSchedule.RecyclingWaste.NextIntendedCollectionDate) > 0, BiffaSchedule.RecyclingWaste.NextIntendedCollectionDate, BiffaSchedule.DomesticWaste.NextIntendedCollectionDate))
						dTemp = BiffaSchedule.FoodWaste.NextActualCollectionDate

						BiffaSchedule.GardenWaste.RefuseType = "Garden"
						sKey = "Gw_Day" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.GardenWaste.Day = sTemp
						sKey = "Gw_Order" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.GardenWaste.Order = CInt(Math.Floor(Val(sTemp)))
						sKey = "Gw_Crew" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.GardenWaste.Crew = sTemp
						'sKey = "Gw_Bin_Type" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.GardenWaste.BinType = sTemp
						sKey = "Gw_Next_Collection" : sTemp = oReader.GetValue(oReader.GetOrdinal(sKey)).ToString : If sTemp <> "" Then BiffaSchedule.GardenWaste.StartDate = Date.Parse(sTemp)
						BiffaSchedule.FoodWaste.CollectionSchedule = "WEEKLY"

						If BiffaSchedule.GardenWaste.StartDate.Ticks = 0 Then BiffaSchedule.GardenWaste.StartDate = CDate(IIf(BiffaSchedule.DomesticWaste.NextIntendedCollectionDate.CompareTo(BiffaSchedule.RecyclingWaste.NextIntendedCollectionDate) > 0, BiffaSchedule.RecyclingWaste.NextIntendedCollectionDate, BiffaSchedule.DomesticWaste.NextIntendedCollectionDate))
						dTemp = BiffaSchedule.GardenWaste.NextActualCollectionDate

					End If

				End If

			End If

		Catch ex As Exception
			Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

		End Try
		If oReader IsNot Nothing Then Try : oReader.Close() : Catch ex As Exception : End Try
		oReader = Nothing
		oSQLCommand = Nothing
		If oSQLConn IsNot Nothing Then Try : oSQLConn.Close() : Catch ex As Exception : End Try
		oSQLConn = Nothing
	End Sub


	Public Function CompareTo(other As clsRefuseCollectionRecord) As Integer Implements IComparable(Of clsRefuseCollectionRecord).CompareTo
		Dim iReturn As Integer
		Try
			iReturn = Me.AddressRecord.AlphaSortKey.CompareTo(other.AddressRecord.AlphaSortKey)

		Catch ex As Exception
			Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

		End Try
		Return iReturn
	End Function
End Class


'<XmlRoot(Namespace:="http://tandridge.gov.uk/TDCRefuseCollection2017/", ElementName:="AddressDetails", IsNullable:=True)>
'Public Class clsLocalAddressRecord
'	Implements IComparable(Of clsLocalAddressRecord)

'	<XmlAttribute>
'	Public UPRN As String
'	Public LPIKey As String
'	<XmlAttribute>
'	Public USRN As String
'	Public LPIDetails As New clsLPIDetails
'	Public BLPUDetails As New clsBLPUDetails
'	Public StreetDetails As New clsStreetDetails
'	Public Address As New clsAddress
'	<XmlAttribute>
'	Public FullAddressAsLine As String
'	<XmlAttribute>
'	Public AddressbasePostal As String
'	Public Parish As String
'	Public Ward As String
'	<XmlArray(ElementName:="Organisations")>
'	<XmlArrayItem(ElementName:="Organisation")>
'	Public Organisations As List(Of clsOrganisationDetails)
'	Public BLPUCoordinates As New clsCoordinates
'	Public StreetCoordinates As New clsStreetCoordinates
'	Public Classification As New clsBLPUClassification
'	Public NamingOriginCode As String
'	Public ParentUPRN As String

'	Public Property AlphaSortKey() As String
'		Get
'			Dim sStreetTemp As String = "", aWords As ArrayList
'			Dim sReturn = ""

'			aWords = New ArrayList(Split(Address.BS7666Format.StreetDescription, " "))
'			For iPos = 0 To aWords.Count - 1
'				sStreetTemp &= Left(aWords(iPos).ToString & "___", 3)
'			Next
'			sReturn = Left(StringInFixedParts(Address.AdminArea, 3) + "______", 6) &
'				Left(StringInFixedParts(Address.PostTown, 3) + "_________", 9) &
'				Left(StringInFixedParts(Address.Town, 3) + "_________", 9) &
'				Left(StringInFixedParts(Address.Locality, 3) + "_________", 9) &
'				Left(StringInFixedParts(Address.BS7666Format.StreetDescription, 4) + "____________", 12) &
'				Left(Address.BS7666Format.PAON.PAOFromToCode + "__________", 10) &
'				Left(StringInFixedParts(Address.BS7666Format.PAON.PAOText, 3) + "______", 6) &
'				Left(Address.BS7666Format.SAON.SAOFromToCode + "__________", 10) &
'				Left(StringInFixedParts(Address.BS7666Format.SAON.SAOText, 3) + "______", 6) &
'				Left(Address.PostCode + "________", 8)

'			Return sReturn
'		End Get
'		Set(value As String)

'		End Set
'	End Property

'	Private Function StringInFixedParts(sValueIn As String, iWordLength As Integer) As String
'		Dim sReturn As String = "", aWords As ArrayList

'		aWords = New ArrayList(Split(sValueIn, " "))
'		For iPos = 0 To aWords.Count - 1
'			sReturn &= Left(aWords(iPos).ToString & Left("__________", iWordLength), iWordLength)
'		Next

'		Return sReturn
'	End Function

'	Public Function CompareTo(other As clsLocalAddressRecord) As Integer Implements IComparable(Of clsLocalAddressRecord).CompareTo
'		Return Me.AlphaSortKey.CompareTo(other.AlphaSortKey)
'	End Function
'End Class

'<XmlType(TypeName:="LPI")>
'Public Class clsLPIDetails
'	<XmlAttribute>
'	Public LPIStartDate As String
'	<XmlAttribute>
'	Public LPIEndDate As String
'	<XmlAttribute>
'	Public LPIEntryDate As String
'	<XmlAttribute>
'	Public LPILastUpdatedDate As String
'	<XmlAttribute>
'	Public LPILogicalStatusCode As String
'	<XmlAttribute>
'	Public LPILogicalStatus As String

'End Class

'<XmlType(TypeName:="BLPU")>
'Public Class clsBLPUDetails
'	<XmlAttribute>
'	Public BLPULogicalStatusCode As String
'	<XmlAttribute>
'	Public BLPULogicalStatus As String
'	<XmlAttribute>
'	Public BLPUStartDate As String
'	<XmlAttribute>
'	Public BLPUEndDate As String
'	<XmlAttribute>
'	Public BLPUEntryDate As String
'	<XmlAttribute>
'	Public BLPULastUpdatedDate As String

'End Class

'<XmlType(TypeName:="Street")>
'Public Class clsStreetDetails
'	<XmlAttribute>
'	Public StreetStartDate As String
'	<XmlAttribute>
'	Public StreetEndDate As String
'	<XmlAttribute>
'	Public StreetEntryDate As String
'	<XmlAttribute>
'	Public StreetLastUpdatedDate As String
'	Public StreetRecordType As String
'	<XmlAttribute>
'	Public StreetRecordTypeCode As String
'	Public StreetState As String
'	<XmlAttribute>
'	Public StreetStateCode As String
'	Public StreetClassification As String
'	<XmlAttribute>
'	Public StreetClassificationCode As String
'	Public StreetSurface As String
'	<XmlAttribute>
'	Public StreetSurfaceCode As String
'End Class


'<XmlType(TypeName:="Organisation")>
'Public Class clsOrganisationDetails
'	Public OrganisationName As String
'	Public OrganisationLegalName As String
'	<XmlAttribute>
'	Public OrganisationKey As String
'	<XmlAttribute>
'	Public OrganisationStartDate As String
'	<XmlAttribute>
'	Public OrganisationEndDate As String
'	<XmlAttribute>
'	Public OrganisationEntryDate As String
'	<XmlAttribute>
'	Public OrganisationLastUpdatedDate As String

'End Class


'<XmlType(TypeName:="Address")>
'Public Class clsAddress
'	Public Locality As String
'	Public Town As String
'	Public PostTown As String
'	Public AdminArea As String
'	Public PostCode As String
'	'Public PostalAddressCode As String
'	Public BS7666Format As New clsBS7666Format
'	Public AddressParts As New clsAddressParts
'	'<TextAddress>
'	<XmlArray>
'	<XmlArrayItem(ElementName:="Addressline")>
'	Public TextAddress As New ArrayList
'	'AddressLine1
'	'AddressLine2
'	'AddressLinen
'	Public FullAddressAsLine As String = ""

'End Class

'<XmlType(TypeName:="BS7666Street")>
'Public Class clsBS7666Format
'	Public SAON As New clsSAON
'	Public PAON As New clsPAON
'	Public StreetDescription As String
'End Class

'<XmlType(TypeName:="SAON")>
'Public Class clsSAON
'	Public SAOText As String
'	Public SAOFrom As String
'	Public SAOTo As String
'	Protected Friend SAOFromToCode As String

'End Class

'<XmlType(TypeName:="PAON")>
'Public Class clsPAON
'	Public PAOText As String
'	Public PAOFrom As String
'	Public PAOTo As String
'	Protected Friend PAOFromToCode As String

'End Class


'<XmlType(TypeName:="AddressParts")>
'Public Class clsAddressParts
'	Public HouseName As String
'	Public StreetHouseNumber As String
'	Public StreetLineNamePart As String
'	Public FullHouseAddress As String

'	<XmlArray>
'	<XmlArrayItem(ElementName:="Addressline")>
'	Public StreetAddress As New ArrayList

'	'<StreetAddressLines>
'	'HouseAddress1
'	'HouseAddressn

'End Class


'<XmlType(TypeName:="Coordinates")>
'Public Class clsCoordinates
'	<XmlAttribute>
'	Public Easting As String
'	<XmlAttribute>
'	Public Northing As String
'	<XmlAttribute>
'	Public Latitude As String
'	<XmlAttribute>
'	Public LatitudeAsString As String
'	<XmlAttribute>
'	Public Longitude As String
'	<XmlAttribute>
'	Public LongitudeAsString As String
'End Class

'<XmlType(TypeName:="StreetCoordinates")>
'Public Class clsStreetCoordinates
'	Public StreetStart As New clsCoordinates
'	Public StreetEnd As New clsCoordinates
'End Class

'Public Class AddressLine
'	Public AddressLine As String
'End Class

'<XmlType(TypeName:="BLPUCLassification")>
'Public Class clsBLPUClassification
'	Public ClassificationCode As String
'	Public PrimaryClass As String
'	Public SecondaryClass As String
'	Public TertiaryClass As String

'End Class
