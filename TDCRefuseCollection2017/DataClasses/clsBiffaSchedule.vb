﻿Imports System.Xml.Serialization
Imports TDCRefuseCollection2017

<XmlType(TypeName:="BiffaSchedule")>
Public Class clsBiffaSchedules

	Public NextScheduledPickup As String = ""

	Public AWC_Calendar As String = ""
	Public AWC_Schedule As String = ""
	Public RoadHazard As String = ""
	Public CommunalSiteID As String = ""
	Public NormalCollectionDay As String = ""

	Public DomesticWaste As New clsBiffaScheduleValues
	Public RecyclingWaste As New clsBiffaScheduleValues
	Public GardenWaste As New clsBiffaScheduleValues
	Public FoodWaste As New clsBiffaScheduleValues
	Public DateToCheck As Date


	<XmlArray("UpcomingWeekdayHolidays"), XmlArrayItem(ElementName:="Holiday")>
	Public NextHolidays As New colHolidays

	Public Sub New()

		DomesticWaste = New clsBiffaScheduleValues
		DomesticWaste.oParent = Me
		RecyclingWaste = New clsBiffaScheduleValues
		RecyclingWaste.oParent = Me
		GardenWaste = New clsBiffaScheduleValues
		GardenWaste.oParent = Me
		FoodWaste = New clsBiffaScheduleValues
		FoodWaste.oParent = Me

	End Sub
End Class

<XmlType(TypeName:="BiffaScheduleDetails")>
Public Class clsBiffaScheduleValues
	Public RefuseType As String = ""
	Public Day As String = ""
	Public Order As Integer = 0
	Public WeekNo As Integer = 1
	Public Crew As String = ""
	Public BinType As String = ""
	Public StartDate As Date
	Protected Friend oParent As clsBiffaSchedules
	'Public NextDateOrig As Date
	Public MessageOfChange As String = ""
	Public CollectionSchedule As String = "WEEKLY"


	Private dNextIntendedDate As Date = Nothing
	Private dNextActualDate As Date = Nothing
	Private dLastSearchDate As Date = Nothing

	Public Property ActualCollectionDay As String
		Get
			Return NextActualCollectionDate.DayOfWeek.ToString
		End Get
		Set(value As String)

		End Set
	End Property

	Public Property IntendedCollectionDay As String
		Get
			Return NextIntendedCollectionDate.DayOfWeek.ToString
		End Get
		Set(value As String)

		End Set
	End Property

	Public Property NextIntendedCollectionDate As Date
		Get
			Dim dReturn As Date
			Dim iDays As Integer, iWeeks As Integer, iDaysDiff As Integer, iPos As Integer
			Dim dLastEvent As Date, iSchedDaysInPeriod As Integer
			Dim oTemp As Object, aScheduleAdjust As StringCollection, aVals As ArrayList
			Dim iDaysScheduleAdjust As Integer, sTemp As String

			Try
				'If StartDate.Ticks = 0 And oParent.DateToCheck.Ticks <> 0 Then StartDate = oParent.DateToCheck
				'StartDate = oParent.DateToCheck

				If StartDate.Ticks <> 0 Then
					If dNextIntendedDate.Ticks <> 0 And dLastSearchDate.CompareTo(StartDate) = 0 Then
						dReturn = dNextIntendedDate

					Else
						dReturn = oParent.DateToCheck

						oTemp = My.Settings.ScheduleAdjusts
						aScheduleAdjust = TryCast(oTemp, StringCollection)
						If aScheduleAdjust IsNot Nothing Then
							For Each sTemp In aScheduleAdjust
								If sTemp <> "" Then
									aVals = New ArrayList(Split(sTemp, "|"))
									If aVals.Count > 1 Then
										If aVals(0).ToString = CollectionSchedule Then
											iDaysScheduleAdjust = CInt(Math.Floor(Val(aVals(1).ToString)))
											dReturn = DateAdd(DateInterval.Day, iDaysScheduleAdjust, dReturn)
											Exit For
										End If
									End If
								End If
							Next
						End If

						iDays = CInt(DateDiff(DateInterval.Day, StartDate, dReturn))
						iWeeks = CInt(Math.Floor(iDays / 7))
						iDaysDiff = iDays Mod 7

						iSchedDaysInPeriod = 7
						If CollectionSchedule = "FORTNIGHTLY" Then iSchedDaysInPeriod = 14
						dLastEvent = DateAdd(DateInterval.Day, CInt(Math.Floor(iDays / iSchedDaysInPeriod)) * iSchedDaysInPeriod, StartDate)
						dReturn = dLastEvent
						If dLastEvent.CompareTo(oParent.DateToCheck) < 0 Then dReturn = DateAdd(DateInterval.Day, iSchedDaysInPeriod, dLastEvent)


						dNextIntendedDate = dReturn

					End If
				End If

			Catch ex As Exception
				Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

			End Try
			Return dReturn
		End Get
		Set(value As Date)

		End Set
	End Property


	Public Property NextActualCollectionDate As Date
		Get
			Dim dReturn As Date = Now
			Dim oTemp As Object, aTemp As ArrayList, aTempCol As StringCollection
			Dim sTemp As String, sTemp2 As String, sTemp3 As String
			Dim iDays As Integer = 0, iWeeks As Integer = 0, iDaysDiff As Integer = 0, iPos As Integer
			Dim iScheduleSteps As Integer, iSchedDaysInPeriod As Integer
			Dim dLastEvent As Date, dNextCollectionEvent As Date
			Dim bSpecial As Boolean = False, aSpecialDays As New SortedList(Of String, Integer) From {{"7 May 2017||Monday", 1}, {"28 May 2017||Monday", 1}, {"27 Aug 2017||Monday", 1}}
			Dim sHolidayKey As String, iSpecialCount As Integer = 0, aDates As String(), sDates As String
			Dim sDateFrom As String, dDateFrom As Date, sDateTo As String, dDateTo As Date
			Dim dTest1 As Date, dTest2 As Date, dTest3 As Date, dTest4 As Date, dEasterDate As Date
			Dim eDay As DayOfWeek, bMondayToNextDay As Boolean = False, dNextWorkday As Date
			Dim dNewYearDay As Date, dNewYearHoliday As Date, dEasterFriday As Date, dEasterMonday As Date
			Dim dChristmasDay As Date, dChristmasHoliday As Date, dBoxingDay As Date, dBoxingHoliday As Date, dWeekEndChristmas As Date
			Dim dHolidayStart As Date, dHolidayEnd As Date, dHolidayWeekEnd As Date
			Dim iScheduleDateShift As Integer = 0

			Try

				'If StartDate.Ticks = 0 And oParent.DateToCheck.Ticks <> 0 Then StartDate = oParent.DateToCheck
				'StartDate = oParent.DateToCheck

				If StartDate.Ticks <> 0 Then
					If dNextActualDate.Ticks <> 0 And dLastSearchDate.CompareTo(StartDate) = 0 Then
						dReturn = dNextActualDate
					Else
						bMondayToNextDay = StringIsTrue(GetMySettingString("BankHoldayWeekNextDay"))

						dLastSearchDate = StartDate
						oParent.NextHolidays.Clear()

						oTemp = GetMySetting("RecordedHolidays")
						If oTemp IsNot Nothing Then
							aTempCol = DirectCast(oTemp, StringCollection)
							aSpecialDays = New SortedList(Of String, Integer)

							For Each sTemp In aTempCol
								iPos = sTemp.LastIndexOf("~")
								If iPos > 0 Then
									sTemp2 = sTemp.Substring(0, iPos)
									sTemp3 = sTemp.Substring(iPos + 1)
									aSpecialDays.Add(sTemp2, CInt(Math.Floor(Val(sTemp3))))
								End If
							Next

						End If

						dReturn = oParent.DateToCheck

						'iDays = CInt(DateDiff(DateInterval.Day, StartDate, dReturn))
						'iWeeks = CInt(Math.Floor(iDays / 7))
						'iDaysDiff = iDays Mod 7

						'iSchedDaysInPeriod = 7
						'If CollectionSchedule = "FORTNIGHTLY" Then iSchedDaysInPeriod = 14
						'dLastEvent = DateAdd(DateInterval.Day, CInt(Math.Floor(iDays / iSchedDaysInPeriod)) * iSchedDaysInPeriod, StartDate)
						'dNextEvent = DateAdd(DateInterval.Day, iSchedDaysInPeriod, dLastEvent)

						dNextCollectionEvent = NextIntendedCollectionDate

						'NextDateOrig = dNextEvent

						' Calculate values for dates

						' New Year

						dNewYearDay = Date.Parse("01 JAN " & dNextCollectionEvent.Year.ToString)
						If dNewYearDay.CompareTo(DateAdd(DateInterval.Day, -7, dReturn)) < 0 Then dNewYearDay = DateAdd(DateInterval.Year, 1, dNewYearDay)
						dNewYearHoliday = dNewYearDay
						dNextWorkday = DateAdd(DateInterval.Day, 1, dNewYearHoliday)
						dHolidayWeekEnd = DateAdd(DateInterval.Day, DayOfWeek.Saturday - dNextWorkday.DayOfWeek, dNextWorkday)

						If dNextIntendedDate.CompareTo(Date.Parse("1 JAN 2000")) > 0 Then

							If dNextIntendedDate.CompareTo(dNewYearHoliday) >= 0 And dNextIntendedDate.CompareTo(dHolidayWeekEnd) <= 0 Then

								If (dNewYearHoliday.DayOfWeek > DayOfWeek.Monday Or bMondayToNextDay) Then
									If dNextIntendedDate.CompareTo(dNewYearHoliday) >= 0 Then
										dNextCollectionEvent = DateAdd(DateInterval.Day, 1, dNextCollectionEvent)
										MessageOfChange = "For New Year holiday your " & RefuseType.ToLower & " refuse will be collected on the next day"

									Else
										dNextCollectionEvent = DateAdd(DateInterval.Day, iSchedDaysInPeriod, dNextCollectionEvent)
										MessageOfChange = "New year day holiday is not being collected on"

									End If
								End If

							End If


							'If dNewYearHoliday.DayOfWeek = DayOfWeek.Monday And dNewYearHoliday.CompareTo(dNextEvent) = 0 Then
							'		dNextEvent = DateAdd(DateInterval.Day, iSchedDaysInPeriod, dNextEvent)
							'		MessageOfChange = "New year day holiday is not being collected on. You will have your " & RefuseType.ToLower & " collection on the next scheduled pickup."
							'	ElseIf dNewYearHoliday.DayOfWeek = DayOfWeek.Sunday And dNewYearHoliday.CompareTo(DateAdd(DateInterval.Day, 1, dNextEvent)) = 0 Then
							'		dNewYearHoliday = DateAdd(DateInterval.day, 1, dNewYearHoliday)
							'		'oParent.NextHolidays.Add(DateAdd(DateInterval.Day, 1, dNewYearHoliday), "New Year Holiday")
							'		dNextEvent = DateAdd(DateInterval.Day, iSchedDaysInPeriod, dNextEvent)
							'		MessageOfChange = "New year day holiday is not being collected on"
							'	ElseIf dNewYearHoliday.DayOfWeek = DayOfWeek.Saturday And dNewYearHoliday.CompareTo(DateAdd(DateInterval.Day, 2, dNextEvent)) = 0 Then
							'		dNewYearHoliday = DateAdd(DateInterval.Day, 2, dNewYearHoliday)
							'		'oParent.NextHolidays.Add(DateAdd(DateInterval.Day, 2, dNewYearHoliday), "New Year Holiday")
							'		dNextEvent = DateAdd(DateInterval.Day, iSchedDaysInPeriod, dNextEvent)
							'		MessageOfChange = "New year day holiday is not being collected on"
							'		'Else
							'		'	oParent.NextHolidays.Add(dNewYearHoliday, "New Year Holiday")
							'	End If
						End If
						oParent.NextHolidays.Add(dNewYearHoliday, "New Year Holiday")



						'Easter
						dEasterDate = GetEasterDate(dReturn.Year)
						If dEasterDate.CompareTo(DateAdd(DateInterval.Day, -7, dReturn)) < 0 Then dEasterDate = GetEasterDate(dReturn.Year + 1)
						dEasterFriday = DateAdd(DateInterval.Day, -2, dEasterDate)
						dEasterMonday = DateAdd(DateInterval.Day, 1, dEasterDate)
						dHolidayWeekEnd = DateAdd(DateInterval.Day, DayOfWeek.Saturday - dEasterMonday.DayOfWeek, dEasterMonday)

						oParent.NextHolidays.Add(dEasterFriday, "Easter Good Friday")
						oParent.NextHolidays.Add(dEasterMonday, "Easter Monday")

						If dNextIntendedDate.CompareTo(Date.Parse("1 Jan 2000")) > 0 Then
							If dNextCollectionEvent.CompareTo(dEasterFriday) >= 0 And dNextCollectionEvent.CompareTo(dHolidayWeekEnd) <= 0 Then

								If dEasterFriday = dNextCollectionEvent Then
									dNextCollectionEvent = DateAdd(DateInterval.Day, 1, dEasterMonday)
									MessageOfChange = "For Easter holiday your " & RefuseType.ToLower & " refuse will be collected on the day after Easter Monday"
								ElseIf dNextCollectionEvent.CompareTo(dEasterDate) > 0 And dNextCollectionEvent.CompareTo(dHolidayWeekEnd) <= 0 Then
									dNextCollectionEvent = DateAdd(DateInterval.Day, 1, dNextCollectionEvent)
									MessageOfChange = "For Easter holiday your " & RefuseType.ToLower & " refuse will be collected on the next day"
								End If
							End If
						End If


						' Christmas
						dChristmasDay = Date.Parse("25 DEC " & dNextCollectionEvent.Year.ToString)
						dBoxingDay = DateAdd(DateInterval.Day, 1, dChristmasDay)
						dChristmasHoliday = dChristmasDay
						dBoxingHoliday = dBoxingDay
						dWeekEndChristmas = DateAdd(DateInterval.Day, 6, dBoxingDay)
						iScheduleDateShift = 0


						If dBoxingDay.DayOfWeek = DayOfWeek.Sunday Then
							dChristmasHoliday = DateAdd(DateInterval.Day, 1, dChristmasDay)
							dBoxingHoliday = DateAdd(DateInterval.Day, 2, dChristmasDay)
							iScheduleDateShift = 1
						ElseIf dBoxingDay.DayOfWeek = DayOfWeek.Saturday Then
							dChristmasHoliday = DateAdd(DateInterval.Day, 2, dChristmasDay)
							dBoxingHoliday = DateAdd(DateInterval.Day, 3, dChristmasDay)
							dWeekEndChristmas = DateAdd(DateInterval.Day, 5, dBoxingHoliday)
							iScheduleDateShift = 1
						ElseIf dTest2.DayOfWeek = DayOfWeek.Friday Then
							dBoxingHoliday = DateAdd(DateInterval.Day, 3, dChristmasDay)
							dWeekEndChristmas = DateAdd(DateInterval.Day, 5, dBoxingHoliday)
						Else
							iScheduleDateShift = 1
							dWeekEndChristmas = DateAdd(DateInterval.Day, DayOfWeek.Saturday - dBoxingHoliday.DayOfWeek, dBoxingHoliday)
						End If

						oParent.NextHolidays.Add(dChristmasHoliday, "Christmas Holiday")
						oParent.NextHolidays.Add(dBoxingHoliday, "Boxing Day Holiday")
						dNextWorkday = DateAdd(DateInterval.Day, 1, dBoxingHoliday)
						If dNextWorkday.DayOfWeek = DayOfWeek.Saturday Then dNextWorkday = DateAdd(DateInterval.Day, 2, dNextWorkday)

						'dTest3 = DateAdd(DateInterval.Day, DayOfWeek.Saturday - dTest2.DayOfWeek, dTest2)
						dTest4 = DateAdd(DateInterval.Day, 6, dWeekEndChristmas)

						If dNextIntendedDate.CompareTo(Date.Parse("1 jan 2000")) > 0 Then

							If dNextIntendedDate.CompareTo(dChristmasDay) >= 0 And dNextIntendedDate.CompareTo(dWeekEndChristmas) <= 0 Then

								If dNextCollectionEvent.CompareTo(dTest4) < 0 And dNextCollectionEvent.CompareTo(dChristmasHoliday) >= 0 And dNextCollectionEvent.CompareTo(dWeekEndChristmas) <= 0 Then
									dNextCollectionEvent = DateAdd(DateInterval.Day, iScheduleDateShift, dNextCollectionEvent)
									MessageOfChange = "For Christmas week your " & RefuseType.ToLower & " refuse will be collected on the day after your usual scheduled day"
								Else

									If dChristmasDay.CompareTo(dNextCollectionEvent) = 0 Then
										dNextCollectionEvent = DateAdd(DateInterval.Day, iSchedDaysInPeriod, dNextCollectionEvent)
										MessageOfChange = "Christmas Day holiday is not being collected on"

									ElseIf dChristmasHoliday.CompareTo(dNextCollectionEvent) = 0 Then
										dNextCollectionEvent = DateAdd(DateInterval.Day, iSchedDaysInPeriod, dNextCollectionEvent)
										MessageOfChange = "Christmas Day holiday is not being collected on"

										'ElseIf dChristmasDay.DayOfWeek = DayOfWeek.Sunday And dChristmasDay.CompareTo(DateAdd(DateInterval.Day, 1, dNextEvent)) = 0 Then
										'	dNextEvent = DateAdd(DateInterval.Day, iSchedDaysInPeriod, dNextEvent)
										'	MessageOfChange = "Christmas Day holiday is not being collected on"

									ElseIf dChristmasDay.DayOfWeek = DayOfWeek.Saturday And dChristmasDay.CompareTo(DateAdd(DateInterval.Day, 2, dNextCollectionEvent)) = 0 Then
										dNextCollectionEvent = DateAdd(DateInterval.Day, iSchedDaysInPeriod, dNextCollectionEvent)
										MessageOfChange = "Christmas Day holiday is not being collected on"
									End If
								End If
							End If
						End If


						iScheduleDateShift = 1
						For Each sHolidayKey In aSpecialDays.Keys
							aDates = Split(sHolidayKey, "|")
							sDateFrom = aDates(0)
							dHolidayStart = Date.Parse(sDateFrom)
							If dHolidayStart.Year < 1900 Then
								dHolidayStart = New Date(NextIntendedCollectionDate.Year, dHolidayStart.Month, dHolidayStart.Day)
							End If
							Do While dHolidayStart.CompareTo(DateAdd(DateInterval.Day, -7, dReturn)) < 0 : dHolidayStart = DateAdd(DateInterval.Year, 1, dHolidayStart) : Loop
							dHolidayEnd = dHolidayStart
							If aDates.Length > 1 Then
								If aDates.Length > 2 Then
									If aDates(2) <> "" Then
										If [Enum].TryParse(aDates(2), eDay) Then
											Do Until eDay = dHolidayStart.DayOfWeek
												dHolidayStart = DateAdd(DateInterval.Day, 1, dHolidayStart)
											Loop
										End If
									End If
								End If

								dHolidayEnd = dHolidayStart
								sDateTo = aDates(1)

								If sDateTo <> "" Then
									dHolidayEnd = Date.Parse(sDateTo)
									If dHolidayEnd < dHolidayStart Then
										dHolidayEnd = New Date(dHolidayStart.Year, dHolidayEnd.Month, dHolidayEnd.Day)
										Do While dHolidayEnd.CompareTo(NextIntendedCollectionDate) < 0 : dHolidayEnd = DateAdd(DateInterval.Year, 1, dHolidayEnd) : Loop
									End If
								End If

							End If

							dNextWorkday = DateAdd(DateInterval.Day, 1, dHolidayStart)
							If dNextWorkday.DayOfWeek = DayOfWeek.Sunday Then dNextWorkday = DateAdd(DateInterval.Day, 1, dNextWorkday)
							If dNextWorkday.DayOfWeek = DayOfWeek.Saturday Then dNextWorkday = DateAdd(DateInterval.Day, 2, dNextWorkday)
							If dNextWorkday.DayOfWeek = DayOfWeek.Friday Then dNextWorkday = DateAdd(DateInterval.Day, 3, dNextWorkday)
							dHolidayWeekEnd = DateAdd(DateInterval.Day, DayOfWeek.Saturday - dNextWorkday.DayOfWeek, dNextWorkday)

							oParent.NextHolidays.Add(dHolidayStart, "Bank Holiday")

							If bMondayToNextDay And dNextCollectionEvent.CompareTo(dHolidayStart) >= 0 And dNextCollectionEvent.CompareTo(dHolidayWeekEnd) <= 0 Then
								dNextCollectionEvent = DateAdd(DateInterval.Day, iScheduleDateShift, dNextCollectionEvent)
								MessageOfChange = "For the Bank Holiday your " & RefuseType.ToLower & " refuse will be collected on the next available day"
							End If
						Next

						If bSpecial Then dNextCollectionEvent = DateAdd(DateInterval.Day, iSpecialCount, dNextCollectionEvent)

						dReturn = dNextCollectionEvent
						dNextActualDate = dReturn
					End If

				End If

			Catch ex As Exception
				Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

			End Try
			Return dReturn
		End Get
		Set(value As Date)

		End Set
	End Property


	Private Function GetEasterDate(YearOf As Integer) As Date
		Dim a As Integer, b As Integer, c As Integer
		Dim d As Integer, e As Integer, f As Integer
		Dim g As Integer, h As Integer, i As Integer
		Dim j As Integer, k As Integer, l As Integer
		Dim m As Integer, n As Integer, p As Integer
		Dim dReturn As Date, Year As Integer

		Try
			' Step 1: Divide the year by 19 and store the
			' remainder in variable A.  Example: If the year
			' is 2000, then A is initialized to 5.

			Year = YearOf
			a = Year Mod 19

			' Step 2: Divide the year by 100.  Store the integer
			' result in B and the remainder in C.

			b = Year \ 100
			c = Year Mod 100

			' Step 3: Divide B (calculated above).  Store the
			' integer result in D and the remainder in E.

			d = b \ 4
			e = b Mod 4

			' Step 4: Divide (b+8)/25 and store the integer
			' portion of the result in F.

			f = (b + 8) \ 25

			' Step 5: Divide (b-f+1)/3 and store the integer
			' portion of the result in G.

			g = (b - f + 1) \ 3

			' Step 6: Divide (19a+b-d-g+15)/30 and store the
			' remainder of the result in H.

			h = (19 * a + b - d - g + 15) Mod 30

			' Step 7: Divide C by 4.  Store the integer result
			' in I and the remainder in K.

			i = c \ 4
			k = c Mod 4

			' Step 8: Divide (32+2e+2i-h-k) by 7.  Store the
			' remainder of the result in L.

			l = (32 + 2 * e + 2 * i - h - k) Mod 7

			' Step 9: Divide (a + 11h + 22l) by 451 and
			' store the integer portion of the result in M.

			m = (a + 11 * h + 22 * l) \ 451

			' Step 10: Divide (h + l - 7m + 114) by 31.  Store
			' the integer portion of the result in N and the
			' remainder in P.

			n = (h + l - 7 * m + 114) \ 31
			p = (h + l - 7 * m + 114) Mod 31

			' At this point p+1 is the day on which Easter falls.
			' n is 3 for March or 4 for April.

			dReturn = DateSerial(Year, n, p + 1)
		Catch ex As Exception
			Dim oErrItem As ErrorItem = RecordErrorGlobal(ex)

		End Try
		Return dReturn
	End Function

End Class

<XmlType("HolidayEvents")>
Public Class colHolidays
	Inherits List(Of clsHolidayEvent)


	Public Overloads Function Add(HolidayDate As Date, HolidayTitle As String) As clsHolidayEvent
		Dim oReturn As New clsHolidayEvent(HolidayDate, HolidayTitle)
		Me.Add(oReturn)
		Return oReturn
	End Function


	Public Overloads Function Add(Holiday As clsHolidayEvent) As clsHolidayEvent
		MyBase.Add(Holiday)
		Return Holiday
	End Function


End Class

<XmlType("HolidayEvent")>
Public Class clsHolidayEvent
	Implements IComparable(Of clsHolidayEvent)

	Public HolidayDate As Date
	Public HolidayTitle As String
	Public Property DayOfWeek As String
		Get
			Return HolidayDate.DayOfWeek.ToString
		End Get
		Set(value As String)

		End Set
	End Property


	Public Sub New()

	End Sub

	Public Sub New(HolidayDateIn As Date, HolidayTitleIn As String)
		Me.HolidayDate = HolidayDateIn
		Me.HolidayTitle = HolidayTitleIn
	End Sub

	Public Function CompareTo(other As clsHolidayEvent) As Integer Implements IComparable(Of clsHolidayEvent).CompareTo
		Return HolidayDate.CompareTo(other.HolidayDate)
	End Function
End Class
