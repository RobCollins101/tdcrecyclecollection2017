Imports System
Imports System.IO

Public Class RunErrors
    '    Inherits ArrayList
    Inherits List(Of ErrorItem)

    Friend sErrorLogFile As String

    Public Function NewError(ByVal ex As System.Exception) As ErrorItem
        Dim oNewErr As ErrorItem = Nothing
        If TypeOf ex Is Threading.ThreadAbortException Then
            Wait(10)
            'Application.DoEvents()
        Else
            oNewErr = New ErrorItem(ex)
            MyBase.Add(oNewErr)
            If sErrorLogFile <> "" Then oNewErr.WriteError(sErrorLogFile)
            If bLocalTest Then MsgBox("ERROR " & oNewErr.ToUserString)

        End If
        Return oNewErr
    End Function

    Public Function NewError(ByVal ex As System.Exception, ByVal sExtra As String) As ErrorItem
        Dim oNewErr As ErrorItem = Nothing
        If TypeOf ex Is Threading.ThreadAbortException Then
            'My.Application.DoEvents()
            Wait(10)
        Else
            oNewErr = New ErrorItem(ex, sExtra)
            MyBase.Add(oNewErr)
            If sErrorLogFile <> "" Then oNewErr.WriteError(sErrorLogFile)
            If bLocalTest Then MsgBox("ERROR " & oNewErr.ToUserString)
        End If

        Return oNewErr
    End Function


    Public Function NewError(ByVal ex As System.Exception, ByVal aExtra As ArrayList) As ErrorItem
        Dim oNewErr As ErrorItem = Nothing
        If TypeOf ex Is Threading.ThreadAbortException Then
            'My.Application.DoEvents()
            Wait(10)
        Else
            oNewErr = New ErrorItem(ex, aExtra)
            MyBase.Add(oNewErr)
            If sErrorLogFile <> "" Then oNewErr.WriteError(sErrorLogFile)
            If bLocalTest Then MsgBox("ERROR " & oNewErr.ToUserString)
        End If

        Return oNewErr
    End Function


    Public Function NewError(ByVal sModuleName As String, ByVal sFunctionName As String, ByVal lLineNo As Long, ByVal sMessage As String,
            ByVal sErrorType As String, ByVal sExtra As String) As ErrorItem
        Dim oNewErr As New ErrorItem()

        oNewErr.sModule = sModuleName
        oNewErr.sFunction = sFunctionName
        If lLineNo > 0 Then oNewErr.sLineNo = lLineNo
        oNewErr.sErrorMsg = sMessage
        oNewErr.sErrorType = sErrorType
        oNewErr.aExtraDetail.Add(sExtra)

        MyBase.Add(oNewErr)
        If sErrorLogFile <> "" Then oNewErr.WriteError(sErrorLogFile)
        If bLocalTest Then MsgBox("ERROR " & oNewErr.ToUserString)

        Return oNewErr
    End Function

    Public Sub WriteErrors()
        Dim iMax As Integer, iPos As Integer, oError As ErrorItem
        Dim wrOut As System.IO.StreamWriter, sLine As String, sType As String
        Dim sTemp As String

        Try

			'If sErrorLogFile = "" Then sErrorLogFile = gsAppPath & IIf(gsAppPath.Substring(gsAppPath.Length) <> IO.Path.DirectorySeparatorChar, IO.Path.DirectorySeparatorChar, "").ToString & My.Application.Info.AssemblyName & "ErrorsOut.txt"
			If sErrorLogFile <> "" Then
                If Not Directory.Exists(Path.GetDirectoryName(sErrorLogFile)) Then Directory.CreateDirectory(Path.GetDirectoryName(sErrorLogFile))
            End If
            If Directory.Exists(Path.GetDirectoryName(sErrorLogFile)) Then
                iMax = MyBase.Count - 1
                If iMax >= 0 Then
                    sTemp = "------------------------------------------------------------------" &
                    "ERRORS - " & Format(Now(), "long")
                    File.AppendAllText(sErrorLogFile, sTemp)
                    'wrOut = New StreamWriter(sErrorDest, True)
                    'wrOut.WriteLine("------------------------------------------------------------------")
                    'wrOut.WriteLine("ERRORS - " & Format(Now(), "long"))
                    For iPos = 0 To iMax
                        sType = MyBase.Item(iPos).GetType.ToString
                        'If sType = "wsTDCOpsCT.ErrorItem" Then
                        oError = MyBase.Item(iPos)
                        oError.WriteError(sErrorLogFile)
                        'sLine = Now().ToString & vbCrLf & oError.ToString
                        'wrOut.WriteLine(sLine)
                        'End If
                    Next iPos
                    sTemp = "------------------------------------------------------------------"
                    File.AppendAllText(sErrorLogFile, sTemp)
                End If

            End If
            'MyBase.Clear()

        Catch ex As Exception
            Dim bDebugSetting As Boolean = False
            Try : bDebugSetting = StringIsTrue(GetMySettingString("InDevTest")) : Catch ex2 As Exception : End Try
            If bDebugSetting Then MsgBox(ex.ToString)

        End Try

    End Sub

    Public Sub ResetErrors()
        'Dim wrOut As System.IO.StreamWriter, sLine As String, sType As String
        'Dim fs As System.IO.File
        If Not Directory.Exists(Path.GetDirectoryName(sErrorLogFile)) Then Directory.CreateDirectory(Path.GetDirectoryName(sErrorLogFile))

		'If sErrorLogFile = "" Then sErrorLogFile = gsAppPath & IIf(gsAppPath.Substring(gsAppPath.Length) <> IO.Path.DirectorySeparatorChar, IO.Path.DirectorySeparatorChar, "").ToString & gsTempPath & My.Application.Info.AssemblyName & "_" & "ErrorsOut.txt"

		'fs.Delete(sErrorDest)
		IO.File.Delete(sErrorLogFile)

    End Sub
End Class



Public Class ErrorItem

    Protected Friend sErrorCode As String = ""
    Protected Friend sErrorMsg As String = ""
    Protected Friend sErrorType As String = ""
    Protected Friend sModule As String = ""
    Protected Friend sFunction As String = ""
    Protected Friend aExtraDetail As New ArrayList
    Protected Friend sLineNo As Long = 0
    Protected Friend oSourceEx As Exception = Nothing
    Protected Friend oStackList As New List(Of String)

    Protected Friend Sub New(ByVal ex As Exception)
        If TypeOf ex Is Threading.ThreadAbortException Then
            'Application.DoEvents()
        Else
            oSourceEx = ex
            sErrorMsg = oSourceEx.Message
            sErrorType = oSourceEx.GetType.ToString
            sModule = Me.GetType.ToString
            If oSourceEx.TargetSite IsNot Nothing AndAlso oSourceEx.TargetSite.ReflectedType IsNot Nothing Then sFunction = oSourceEx.TargetSite.ReflectedType.ToString & "  "
            sFunction &= oSourceEx.Source

            StackToString()
            LogFileEntry(ToString, True)
        End If

    End Sub

    Protected Friend Sub New(ByVal ex As Exception, ByVal aExtra As ArrayList)
        If TypeOf ex Is Threading.ThreadAbortException Then
            'Application.DoEvents()
        Else
            oSourceEx = ex
            sErrorMsg = oSourceEx.Message
            sErrorType = oSourceEx.GetType.ToString
            sModule = Me.GetType.ToString
            If oSourceEx.TargetSite IsNot Nothing AndAlso oSourceEx.TargetSite.ReflectedType IsNot Nothing Then sFunction = oSourceEx.TargetSite.ReflectedType.ToString & "  "
            sFunction &= oSourceEx.Source
            aExtraDetail.AddRange(aExtra)
            StackToString()
            LogFileEntry(ToString, True)
        End If

    End Sub

    Protected Friend Sub New(ByVal ex As Exception, ByVal Extra As String)
        If TypeOf ex Is Threading.ThreadAbortException Then
            'Application.DoEvents()
        Else
            oSourceEx = ex
            sErrorMsg = oSourceEx.Message
            sErrorType = oSourceEx.GetType.ToString
            sModule = Me.GetType.ToString
            If oSourceEx.TargetSite IsNot Nothing AndAlso oSourceEx.TargetSite.ReflectedType IsNot Nothing Then sFunction = oSourceEx.TargetSite.ReflectedType.ToString & "  "
            sFunction &= oSourceEx.Source
            aExtraDetail.Add(Extra)
            StackToString()
            LogFileEntry(ToString, True)
        End If
    End Sub

    Protected Friend Sub New()
    End Sub

    Public Overrides Function ToString() As String
        Dim sOut As String = ""
        sOut = sErrorType & " Error located in module " & sModule & " in function " & sFunction
        If sLineNo > 0 Then sOut = sOut & " at line " & sLineNo.ToString
        If aExtraDetail.Count > 0 Then sOut = sOut & vbCrLf & " " & Join(aExtraDetail.ToArray, vbCrLf)
        sOut = sOut & vbCrLf & sErrorMsg
        StackToString()
        For Each sTemp In Me.oStackList
            sOut = sOut & vbCrLf & "   " & sTemp
        Next
        Return sOut
    End Function


    Public Function ToUserString() As String
        Dim sOut As String = ""
        Dim bDebugSetting As Boolean = False
        Try : bDebugSetting = StringIsTrue(GetMySettingString("InDevTest")) : Catch ex As Exception : End Try
        If bDebugSetting Then
            sOut = sErrorType & " Error located in module " & sModule & " in function " & sFunction
            If sLineNo > 0 Then sOut = sOut & " at line " & sLineNo.ToString
        End If
        If aExtraDetail.Count > 0 Then sOut = sOut & vbCrLf & " " & Join(aExtraDetail.ToArray, vbCrLf)
        sOut = sOut & vbCrLf & sErrorMsg
        StackToString()
        For Each sTemp In Me.oStackList
            sOut = sOut & vbCrLf & "   " & sTemp
        Next
        Return sOut
    End Function


    Private Sub StackToString()
        Dim sResponse As String, oFS As StackFrame, oFrames As StackFrame()
        Dim sTemp As String
        Dim oStack As New StackFrame(2, True)
        Dim oST As New StackTrace(oStack)
        Try
            oStackList = New List(Of String)
            If oSourceEx IsNot Nothing Then
                oStack = New StackFrame(2, True)
                oST = New StackTrace(oStack)
                oFrames = oST.GetFrames
                For Each oFS In oFrames
                    sTemp = "File : " & oFS.GetFileName
                    sResponse = sTemp & Space(50 - sTemp.Length)
                    sTemp = "  Line " & oFS.GetFileLineNumber
                    sResponse &= sTemp & Space(12 - sTemp.Length)
                    sTemp = "  Col " & oFS.GetFileColumnNumber
                    sResponse &= sTemp & Space(10 - sTemp.Length)
                    sTemp = "  Procedure : " & oFS.GetMethod.Name
                    sResponse &= sTemp & Space(50 - sTemp.Length)
                    oStackList.Add(sResponse)
                Next
				Try : oStackList.AddRange(oSourceEx.StackTrace.Replace(" at ", vbCrLf & "at ").Replace(" in ", vbCrLf & "in ").Split(CType(vbCrLf, Char()))) : Catch ex2 As Exception : End Try
            End If

        Catch ex As Exception

        End Try
    End Sub


    Public Sub WriteError(sErrLogFile As String)
        Dim sTemp As String ', wrOut As StreamWriter
        Try
            If sErrLogFile <> "" Then
                If Not Directory.Exists(Path.GetDirectoryName(sErrLogFile)) Then Directory.CreateDirectory(Path.GetDirectoryName(sErrLogFile))
            End If
            If Directory.Exists(Path.GetDirectoryName(sErrLogFile)) Then
                'wrOut = New StreamWriter(sErrLogFile, True)
                sTemp = "------------------------------------------------------------------" & vbCrLf &
                     Me.ToString & vbCrLf &
                     "------------------------------------------------------------------"
                File.AppendAllText(sErrLogFile, sTemp)
                'wrOut.WriteLine("------------------------------------------------------------------")
                'wrOut.WriteLine(Me.ToString)
                'wrOut.WriteLine("------------------------------------------------------------------")
                'wrOut.Flush()
                'wrOut.Close()
                'wrOut.Dispose()
                'wrOut = Nothing
            End If

        Catch ex As Exception
        End Try
    End Sub

End Class