Imports System.Threading
Imports Microsoft.Win32
Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Configuration
Imports System.Net
Imports System.Text
Imports System.Xml
Imports System.Security

Module basGlobalFunctions

	Public Sub Wait(ByVal ms As Integer)
		Using wh As New ManualResetEvent(False)
			wh.WaitOne(ms)
		End Using
	End Sub

	Public Function MassReplace(ByVal StringIn As String, ByVal ReplaceList As String, ByVal ReplaceWith As String) As String
		Dim sReturn As String = "", sNextReplaceList As String = Mid(ReplaceList, 2), sReplaceChar As String
		Try

			sReplaceChar = Left(ReplaceList, 1)
			If ReplaceWith <> sReplaceChar Then
				sReturn = Replace(StringIn, sReplaceChar, ReplaceWith)
			Else
				sReturn = StringIn
			End If

			If Len(ReplaceList) > 1 Then
				sNextReplaceList = Mid(ReplaceList, 2)
				sReturn = MassReplace(sReturn, sNextReplaceList, ReplaceWith)
			End If

		Catch ex As Exception

			Dim oError As ErrorItem = glErrors.NewError(ex)
			'If bLocalTest Then MsgBox("ERROR " & oError.ToString)

		End Try
		Return sReturn
	End Function


	Public Function Strim(ByVal sString As String, Optional ByVal TrimChar As String = " ") As String
		Try
			Dim iTries As Integer = CInt(Math.Sqrt(sString.Length) + 1)

			For iCnt As Integer = 1 To iTries
				sString = Replace(sString, TrimChar & TrimChar, TrimChar)
			Next

			If Left(sString, 1) = TrimChar Then sString = sString.Substring(2)
			If sString > "" AndAlso Right(sString, 1) = TrimChar Then sString = Left(sString, sString.Length - 1)

		Catch ex As Exception

		End Try

		Return sString

	End Function

	Public Function bytesToString(ByVal lValIn As Long) As String
		Dim iScale As Long = 0, iNewScale As Integer = -1, lTemp As Long
		Dim dNumPart As Double = lValIn, lMin As Long, lMax As Long, sReturn As String = ""

		Try

			Do
				iNewScale = iNewScale + 1
				lTemp = CLng(1024 ^ iNewScale)
				lMin = lTemp * 3 : If lMin < 1024 Then lMin = 0
				lMax = CLng(1024 ^ (iNewScale + 1) * 3)
			Loop While lValIn > lMax

			dNumPart = CDbl(lValIn) / lTemp

			Select Case iNewScale
				Case 0 : sReturn = lValIn & " Byte" & IIf(lValIn > 1, "s", "").ToString
				Case 1 : sReturn = dNumPart.ToString("###0.###") & " Kb"
				Case 2 : sReturn = dNumPart.ToString("###0.###") & " Mb"
				Case Else : sReturn = dNumPart.ToString("######0.###") & " Gb"
			End Select


		Catch ex As Exception

			Dim oError As ErrorItem = glErrors.NewError(ex)
			'If bLocalTest Then MsgBox("ERROR " & oError.ToString)

		End Try

		Return sReturn
	End Function

	'Public Function BytesFromString(ByVal sValIn As String) As Long
	'    Dim lReturn As Long = 0, dVal As Double, sSize As String
	'    Dim oRegex As New System.Text.RegularExpressions.Regex(My.Settings.RegexFileSize)
	'    Dim oRegexMatches As System.Text.RegularExpressions.Match

	'    Try
	'        sValIn = Replace(Trim(UCase(sValIn)), " ", "")
	'        If sValIn = "" Then
	'            dVal = 0
	'        Else
	'            If IsNumeric(sValIn) Then
	'                dVal = CDbl(sValIn)
	'            Else
	'                If oRegex.IsMatch(sValIn) Then
	'                    oRegexMatches = oRegex.Match(sValIn)
	'                    If IsNumeric(oRegexMatches.Groups("Value").Value) Then
	'                        dVal = CDbl(oRegexMatches.Groups("Value").Value)
	'                        sSize = oRegexMatches.Groups("Quantifier").Value

	'                        If Left(sSize, 1) = "K" Then
	'                            dVal = dVal * 1024
	'                        ElseIf Left(sSize, 1) = "M" Then
	'                            dVal = dVal * 1024 ^ 2
	'                        ElseIf Left(sSize, 1) = "G" Then
	'                            dVal = dVal * 1024 ^ 3
	'                        End If

	'                    End If
	'                End If
	'            End If
	'        End If

	'        lReturn = CLng(dVal)

	'    Catch ex As Exception

	'        Dim oError As ErrorItem = glErrors.NewError(ex)
	'        'If bLocalTest Then MsgBox("ERROR " & oError.ToString)

	'    End Try

	'    Return lReturn
	'End Function


	Public Sub LogFileEntry(ByVal sText As String, Optional ByVal ForErrorLogging As Boolean = False)
		Dim oLogDirectory As IO.DirectoryInfo, oLogFile As IO.FileInfo
		Dim oWriter As IO.StreamWriter, sLogPath As String

		Try
			If gsLogPath > "" Then
				'sLogPath = ApplyGeneralTemplate(gsLogPath, Now)
				sLogPath = gsLogPath
				oLogFile = New IO.FileInfo(sLogPath)
				oLogDirectory = New IO.DirectoryInfo(oLogFile.DirectoryName)

				If Not oLogDirectory.Exists Then oLogDirectory.Create()

				If oLogDirectory.Exists Then
					File.AppendAllText(sLogPath, sText)

					'oWriter = oLogFile.AppendText()
					'oWriter.WriteLine(sText)
					'oWriter.Flush()
					'oWriter.Close()
					'oWriter.Dispose()
				End If
				oLogDirectory = Nothing
			End If

		Catch ex As Exception

			If Not ForErrorLogging Then
				Dim oError As ErrorItem = glErrors.NewError(ex)
				'If bLocalTest Then MsgBox("ERROR " & oError.ToString)
			Else
				'WatchFileConsole.txtCurrentActivity.Text = ex.ToString & vbCrLf & WatchFileConsole.txtCurrentActivity.Text
			End If

		End Try

	End Sub

	'Public Function ApplyGeneralTemplate(ByVal sStringIn As String, ByVal oDate As Date) As String
	'    Dim sReturn As String = sStringIn
	'    Dim sSourceName As String = "", sDestName As String = ""
	'    Dim sRelativeSourceFolder As String = "", sRelativeSourceFile As String = ""
	'    Dim oValReplacer As clsValueSubstitutions, oVals As New Hashtable

	'    Try

	'        If oDate.Ticks = 0 Then oDate = Now

	'        oValReplacer = New clsValueSubstitutions
	'        oValReplacer.dPassedDate = oDate
	'        oValReplacer.OriginalString = sStringIn
	'        sReturn = oValReplacer.Result
	'        oValReplacer = Nothing

	'    Catch ex As Exception

	'        Dim oError As ErrorItem = glErrors.NewError(ex)
	'        'If bLocalTest Then MsgBox("ERROR " & oError.ToString)

	'    End Try

	'    Return sReturn
	'End Function


	Public Function IsSafeToOpen(sFilePath As String) As Boolean
		Dim oTemp As Object, aExtnList As ArrayList, sExtn As String
		Dim bReturn As Boolean
		Try
			oTemp = GetMySetting("DoNotRunExtensions")
			If oTemp Is Nothing Then
				aExtnList = New ArrayList() From {{"BAT"}, {"BIN"}, {"CMD"}, {"COM"}, {"CPL"}, {"EXE"}, {"GADGET"}, {"INF1"}, {"INS"}, {"INX"}, {"ISU"}, {"JOB"}, {"JSE"}, {"LNK"}, {"MSC"}, {"MSI"}, {"MSP"}, {"MST"}, {"PAF"}, {"PIF"}, {"PS1"}, {"REG"}, {"RGS"}, {"SCR"}, {"SCT"}, {"SHB"}, {"SHS"}, {"U3P"}, {"VB"}, {"VBE"}, {"VBS"}, {"VBSCRIPT"}, {"WS"}, {"WSF"}}
			Else
				aExtnList = New ArrayList(DirectCast(oTemp, Specialized.StringCollection))
			End If
			sExtn = IO.Path.GetExtension(sFilePath)
			bReturn = Not (aExtnList.Contains(sExtn.Replace(",", "")))

		Catch ex As Exception
			Dim oError As ErrorItem = glErrors.NewError(ex)
		End Try
		Return bReturn
	End Function


	Public Function GetMIMEType(filepath As String) As String
		'Dim regPerm As Security.Permissions.RegistryPermission = New Security.Permissions.RegistryPermission(Security.Permissions.RegistryPermissionAccess.Read, "\\HKEY_CLASSES_ROOT")
		Dim classesRoot As RegistryKey = Registry.ClassesRoot
		Dim fi = New FileInfo(filepath)
		Dim dotExt As String = LCase(fi.Extension)
		Dim typeKey As RegistryKey = classesRoot.OpenSubKey("MIME\Database\Content Type")
		Dim keyname As String, sReturn As String = "application/octet-stream"

		For Each keyname In typeKey.GetSubKeyNames()
			Dim curKey As RegistryKey = classesRoot.OpenSubKey("MIME\Database\Content Type\" & keyname)
			If curKey.GetValue("Extension").ToString.ToLower = dotExt.ToLower Then
				sReturn = keyname
				Exit For
			End If
		Next
		Return sReturn
	End Function


	'Friend Sub ReportToUser(sMessage As String, IsErrorMessage As Boolean, IsVerboseMessage As Boolean, Optional bAllowAbort As Boolean = True)
	'    Dim sMsgOut As String, oMsgResponse As MessageBoxResult
	'    Try
	'        sMsgOut = Replace(Replace(sMessage, "<br/>", vbCrLf), "<br>", vbNewLine)
	'        If gbHideGUI Then
	'            If IsErrorMessage Or gbVerboseCLIFeedback Or Not IsVerboseMessage Then
	'                If IsErrorMessage Then
	'                    'Console.Error.WriteLine(sMessage)
	'                    ReportErrToCLI(sMessage)
	'                    If bAllowAbort Then goManageProcess.stopProcessing = True

	'                Else
	'                    'Console.Out.WriteLine(sMessage)
	'                    ReportToCLI(sMessage, IsVerboseMessage)
	'                End If
	'            ElseIf IsErrorMessage And bAllowAbort Then
	'                goManageProcess.stopProcessing = True
	'            End If
	'        Else
	'            'If IsErrorMessage Or Not IsVerboseMessage Then
	'            If IsErrorMessage Then
	'                If bAllowAbort Then
	'                    sMsgOut &= vbCrLf & "Continue?"
	'                    oMsgResponse = MsgBox(sMsgOut, MsgBoxStyle.Critical + MsgBoxStyle.YesNo, "ERROR in " & My.Application.Info.ProductName)
	'                    If oMsgResponse = MessageBoxResult.No Then goManageProcess.stopProcessing = True

	'                Else
	'                    MsgBox(sMsgOut, MsgBoxStyle.Critical)
	'                End If
	'            Else
	'                oMsgResponse = MsgBox(sMsgOut, "Message from " & My.Application.Info.ProductName)
	'            End If
	'            'End If

	'        End If

	'    Catch ex As Exception

	'    End Try
	'End Sub



	Friend Sub ReportErrorGlobal(oErrItem As ErrorItem)
		Try
			If gsErrorLogPath <> "" Then
				Try : IO.File.AppendAllText(gsErrorLogPath, vbCrLf & Now.ToString("o") & vbCrLf) : Catch ex2 As Exception : End Try
				Try : IO.File.AppendAllText(gsErrorLogPath, oErrItem.ToString) : Catch ex2 As Exception : End Try
				Try : IO.File.AppendAllText(gsErrorLogPath, oErrItem.oSourceEx.StackTrace.Replace(" at ", vbCrLf & "at ").Replace(" in ", vbCrLf & "in ") & vbCrLf) : Catch ex2 As Exception : End Try

			End If

			'If gbHideGUI Then
			'    'Console.Error.WriteLine(Replace(Replace(oErrItem.ToString, "<br />", vbCrLf), "<br/>", vbCrLf))
			'    ReportErrToCLI(Replace(Replace(oErrItem.ToString, "<br />", vbCrLf), "<br/>", vbCrLf))
			'Else
			'    MsgBox(Replace(Replace(oErrItem.ToUserString, "<br />", vbCrLf), "<br/>", vbCrLf), MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal + vbMsgBoxSetForeground, "Error detected")
			'End If

		Catch ex As Exception
			'If gbHideGUI Then
			'    Console.Error.WriteLine(Replace(Replace(ex.ToString, "<br />", vbNewLine), "<br/>", vbNewLine))
			'Else
			'    MsgBox(ex.ToString, MsgBoxStyle.Critical)

			'End If

		End Try
	End Sub



	Friend Function RecordErrorGlobal(exIn As Exception, Optional ExtraDets As ArrayList = Nothing) As ErrorItem
		Dim oErr As ErrorItem = Nothing
		Try

			oErr = glErrors.NewError(exIn)
			If ExtraDets IsNot Nothing AndAlso ExtraDets.Count > 0 Then oErr.aExtraDetail.AddRange(ExtraDets)
			gsErr &= "<br /><br />" & Replace(oErr.ToString, vbCrLf, "<br />")

			ReportErrorGlobal(oErr)

		Catch ex2 As Exception
			oErr = New ErrorItem(exIn)
		End Try
		Return oErr
	End Function



	'Friend Sub RecordErrorGlobal(exIn As Exception, ExtraDets As ArrayList)
	'    Try

	'        Dim oErr As ErrorItem = glErrors.NewError(exIn)
	'        If ExtraDets IsNot Nothing AndAlso ExtraDets.Count > 0 Then oErr.aExtraDetail.AddRange(ExtraDets)
	'        gsErr &= "<br /><br />" & Replace(oErr.ToString, vbCrLf, "<br />")

	'        ReportErrorGlobal(oErr)

	'    Catch ex2 As Exception

	'    End Try

	'End Sub



	'Public Function MakeStringFileNameSafe(NameString) As String
	'    Dim sReplacementChar As String = "", sChar As Char
	'    Dim sReturn As String = NameString
	'    Try
	'        For Each sChar In IO.Path.GetInvalidFileNameChars
	'            sReturn = sReturn.Replace(sChar, sReplacementChar)
	'        Next
	'    Catch ex As Exception
	'        Dim oError As ErrorItem = glErrors.NewError(ex)

	'    End Try
	'    Return sReturn
	'End Function


	Public Function GetMySettingString(SettingName As String) As String
		Dim oTemp As Object, sReturn As String = ""
		Try

			oTemp = GetMySetting(SettingName)
			If oTemp IsNot Nothing Then sReturn = oTemp.ToString

		Catch ex As Exception
			Dim oError As ErrorItem = glErrors.NewError(ex)

		End Try
		Return sReturn
	End Function


	Public Function GetMySetting(SettingName As String) As Object
		Dim oReturn As Object = Nothing
		Try
			' See http://msdn.microsoft.com/en-us/library/ms178688%28v=vs.100%29.aspx
			' and http://forums.asp.net/t/1124253.aspx
			' These relate to the web.config section 	<configuration><appSettings>
			Dim oAppSettings = ConfigurationSettings.AppSettings

			Try : oReturn = oAppSettings(SettingName) : Catch e As Exception : End Try
			If oReturn Is Nothing Then Try : oReturn = My.Settings.Item(SettingName) : Catch e As Exception : End Try
			If oReturn Is Nothing Then oReturn = ""

		Catch ex As Exception
			Dim oError As ErrorItem = glErrors.NewError(ex)
			'If bLocalTest Then MsgBox("ERROR " & oError.ToString)

		End Try
		Return oReturn
	End Function


	Public Function DuplicateSortedList(oSource As SortedList(Of String, String)) As SortedList(Of String, String)
		Dim oReturn As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
		Dim oKey As String
		Try

			For Each oKey In oSource.Keys
				oReturn.Add(oKey, oSource.Item(oKey))
			Next

		Catch ex As Exception
			Dim oError As ErrorItem = glErrors.NewError(ex)
			'If bLocalTest Then MsgBox("ERROR " & oError.ToString)

		End Try
		Return oReturn
	End Function


	Public Function FileToURLPath(sSource As String) As String
		Dim sReturn As String = sSource, oURI As Uri
		Try
			If sSource <> "" Then
				oURI = New Uri(sSource)
				'sReturn = Replace(sReturn, "\", "/")
				'sReturn = "file:" & IIf(Left(sReturn, 2) <> "//", "//", "").ToString & IIf(sReturn.Substring(1, 1) = ":", "/", "").ToString & sReturn
				sReturn = oURI.AbsoluteUri
			End If
		Catch ex As Exception
			Dim oError As ErrorItem = glErrors.NewError(ex)
			'If bLocalTest Then MsgBox("ERROR " & oError.ToString)

		End Try
		Return sReturn
	End Function


	Public Function StringIsTrue(StrVal As String) As Boolean
		Dim bReturn As Boolean = False
		Try

			If IsNumeric(StrVal) And StrVal <> "" Then
				bReturn = Val(StrVal) <> 0
			Else
				bReturn = (StrVal = "True" Or StrVal = "T" Or StrVal = "Yes" Or StrVal = "Y")
			End If
		Catch ex As Exception
			Dim oError As ErrorItem = glErrors.NewError(ex)
			'If bLocalTest Then MsgBox("ERROR " & oError.ToString)
		End Try
		Return bReturn
	End Function


	Public Function ValidPath(sPath As String) As String
		Dim sRegFile As String = "", sRegRelativeFile As String = ""
		Dim oTemp As Object, bValid As Boolean = False
		Dim sReturn As String = sPath, sCurrpath = My.Computer.FileSystem.CurrentDirectory
		Try
			If sPath.Length < 255 Then
				sRegFile = GetMySettingString("FilePathRegex")
				sRegRelativeFile = GetMySettingString("FilePathIncRelativeRegex")
				If sRegFile <> "" And Regex.IsMatch(sReturn, sRegFile) Then bValid = True
				If Not bValid And sRegRelativeFile <> "" And Regex.IsMatch(sReturn, sRegRelativeFile) Or sReturn.LastIndexOf(IO.Path.DirectorySeparatorChar) < 0 Then
					sReturn = sCurrpath & IIf(sCurrpath.Substring(gsCurrentPath.Length - 1) <> IO.Path.DirectorySeparatorChar, IO.Path.DirectorySeparatorChar, "").ToString & sPath
				End If

				If sRegFile <> "" And Regex.IsMatch(sReturn, sRegFile) Then bValid = True
			End If
			If Not bValid Then sReturn = ""

		Catch ex As Exception
			Dim oError As ErrorItem = glErrors.NewError(ex)

		End Try
		Return sReturn
	End Function


	'Public Function LoadIfFile(sData As String, aFilePathSpecific As SortedList(Of String, String), aHTMLSpecific As SortedList(Of String, String)) As String
	'    Dim oSubstitutes As clsValueSubstitutions, sResult As String = ""
	'    Dim oFileInfo As FileInfo, oReader As StreamReader, sTemp As String

	'    Try

	'        If sData.Length < 255 And (sData.LastIndexOf(IO.Path.DirectorySeparatorChar) > 0 Or sData.LastIndexOf("/") > 0) Then
	'            oSubstitutes = New clsValueSubstitutions(sData, aFilePathSpecific)
	'        Else
	'            oSubstitutes = New clsValueSubstitutions(sData, aHTMLSpecific)
	'        End If
	'        sResult = oSubstitutes.Result
	'        sTemp = ValidPath(sResult)
	'        'If Regex.IsMatch(sResult, GetMySetting("FilePathRegex").ToString) Then
	'        If sTemp <> "" Then
	'            sResult = sTemp
	'            oFileInfo = New FileInfo(sResult)
	'            If oFileInfo.Exists Then
	'                oReader = oFileInfo.OpenText
	'                sTemp = oReader.ReadToEnd
	'                oReader.Close()
	'                oSubstitutes = New clsValueSubstitutions(sData, aHTMLSpecific)
	'                sResult = oSubstitutes.GetSubstitutionForString(sTemp)
	'            End If
	'        End If

	'    Catch ex As Exception
	'        Dim oError As ErrorItem = glErrors.NewError(ex)
	'        'If bLocalTest Then MsgBox("ERROR " & oError.ToString)

	'    End Try

	'    Return sResult
	'End Function


	Public Function LoadFile(sPath As String) As String
		Dim sReturn As String = sPath, sTemp As String = ""
		Dim oFileInfo As FileInfo
		Try
			sTemp = GetMySettingString("FilePathRegex")
			If Regex.IsMatch(sPath, sTemp) Then
				'If File.Exists(sPath) Then sReturn = File.ReadAllText(sPath)
				oFileInfo = New FileInfo(sPath)
				If oFileInfo.Exists Then sReturn = File.ReadAllText(sPath)
			End If


		Catch ex As Exception
			Dim oError As ErrorItem = glErrors.NewError(ex)
			'If bLocalTest Then MsgBox("ERROR " & oError.ToString)
		End Try
		Return sReturn
	End Function




	'Function GetCommandLineArgs() As Hashtable
	'    ' Declare variables.
	'    Dim separators As String = " "
	'    Dim commands As String = Microsoft.VisualBasic.Command()
	'    Dim args() As String = commands.Split(separators.ToCharArray)
	'    Dim aReturn As New Hashtable

	'    Dim sArg As String
	'    Dim sArgKey As String = ""
	'    Dim sArgValue As String = ""
	'    Dim bInQuotes As Boolean = False

	'    Dim iPos As Integer, iMax As Integer


	'    Try

	'        For iPos = 0 To UBound(args)

	'            sArg = args(iPos)
	'            If bInQuotes Then
	'                If Right(sArg, 1) = """" Then
	'                    sArgValue = IIf(sArgValue = "", "", sArgValue & " ").ToString & sArg
	'                    If sArgKey = "" And aReturn.ContainsKey("") Then sArgKey = "Arg" & iPos.ToString
	'                    If sArgValue = """""" Then
	'                        sArgValue = ""
	'                    Else
	'                        sArgValue = Mid(sArgValue, 2, sArgValue.Length - 2)
	'                    End If
	'                    If sArgValue.Length > 2 Then
	'                        If Left(sArgValue, 2) <> """""" And Left(sArgValue, 1) = """" And Right(sArgValue, 1) = """" And sArgValue.Length > 2 Then
	'                            sArgValue = sArgValue.Substring(1, sArgValue.Length - 2).Replace("""""", """")
	'                        End If
	'                    End If
	'                    If aReturn.ContainsKey(sArgKey) Then
	'                        If sArgValue <> "" And gsParamListDivider <> "" Then aReturn.Item(sArgKey) = aReturn.Item(sArgKey).ToString & gsParamListDivider & sArgValue
	'                    Else
	'                        aReturn.Add(sArgKey, sArgValue)
	'                    End If
	'                    sArgKey = ""
	'                    sArgValue = ""
	'                    bInQuotes = False
	'                Else
	'                    sArgValue = IIf(sArgValue = "", "", sArgValue & " ").ToString & sArg
	'                End If
	'            Else
	'                If Left(sArg, 1) = "-" Or Left(sArg, 1) = "/" Then
	'                    If sArgValue <> "" Or sArgKey <> "" Then
	'                        If sArgKey = "" And aReturn.ContainsKey("") Then sArgKey = "Arg" & iPos.ToString
	'                        If Left(sArgValue, 2) <> """""" And Left(sArgValue, 1) = """" And Right(sArgValue, 1) = """" And sArgValue.Length > 2 Then sArgValue = sArgValue.Substring(1, sArgValue.Length - 2).Replace("""""", """")
	'                        If aReturn.ContainsKey(sArgKey) Then
	'                            If sArgValue <> "" And gsParamListDivider <> "" Then aReturn.Item(sArgKey) = aReturn.Item(sArgKey).ToString & gsParamListDivider & sArgValue
	'                        Else
	'                            aReturn.Add(sArgKey, sArgValue)
	'                        End If
	'                    End If
	'                    sArgValue = ""
	'                    sArgKey = Replace(Left(sArg, 2), "/", "").Replace("-", "").ToUpper
	'                    If sArgKey <> sArg Then sArgValue = LTrim(RTrim(Mid(sArg, 3)))
	'                    If Left(sArgValue, 1) = """" And Right(sArgValue, 1) = """" Then
	'                        sArgValue = Mid(sArgValue, 2, sArgValue.Length - 2)
	'                    ElseIf Left(sArgValue, 1) = """" Then
	'                        bInQuotes = True
	'                    End If
	'                Else
	'                    sArgValue = IIf(sArgValue = "", "", sArgValue & " ").ToString & sArg
	'                    If Left(sArgValue, 1) = """" And Not Right(sArgValue, 1) = """" Then
	'                        bInQuotes = True
	'                    End If
	'                End If
	'            End If

	'            If glErrors.Count > 0 Then Exit For

	'        Next
	'        If sArgValue <> "" Then
	'            If bInQuotes And Left(sArgValue, 1) = """" And Right(sArgValue, 1) = """" Then sArgValue = Mid(sArgValue, 2, sArgValue.Length - 2)
	'            If sArgKey = "" And aReturn.ContainsKey("") Then sArgKey = "Arg" & UBound(args).ToString
	'            If aReturn.ContainsKey(sArgKey) Then
	'                If gsParamListDivider <> "" Then aReturn.Item(sArgKey) = aReturn.Item(sArgKey).ToString & gsParamListDivider & sArgValue
	'            Else
	'                aReturn.Add(sArgKey, sArgValue)
	'            End If
	'        ElseIf sArgKey <> "" And Not aReturn.ContainsKey(sArgKey) Then
	'            aReturn.Add(sArgKey, sArgValue)
	'        End If

	'        iMax = aReturn.Keys.Count - 1
	'        For iPos = 0 To iMax
	'            'For Each sArgKey In aReturn.Keys
	'            sArgKey = aReturn.Keys(iPos).ToString
	'            sArgValue = aReturn.Item(sArgKey).ToString
	'            If Left(sArgValue, 2) <> """""" And Left(sArgValue, 1) = """" And Right(sArgValue, 1) = """" And sArgValue.Length > 2 Then
	'                aReturn.Item(sArgKey) = sArgValue.Substring(1, sArgValue.Length - 2).Replace("""""", """")
	'            End If
	'        Next

	'    Catch ex As Exception
	'        Dim oError As ErrorItem = glErrors.NewError(ex)

	'    End Try


	'    Return aReturn

	'End Function




	Public Function NewAttr(oDom As XmlDocument, sAttrName As String, sAttrValue As String) As XmlAttribute
		Dim oReturn As XmlAttribute = Nothing
		Try
			oReturn = oDom.CreateAttribute(sAttrName)
			oReturn.Value = sAttrValue
		Catch ex As Exception
			Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
		End Try
		Return oReturn
	End Function

	Public Function NewElement(oDom As XmlDocument, sElementName As String, sElementValue As String) As XmlElement
		Dim oReturn As XmlElement = Nothing
		Try
			oReturn = oDom.CreateElement(sElementName)
			oReturn.InnerText = sElementValue
		Catch ex As Exception
			Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
		End Try
		Return oReturn
	End Function

	Public Function GetValueOfNode(sFieldName As String, oXMLParentElement As XmlElement) As String
		Dim oXMLTempNode As XmlNode, oXMLTempElem As XmlElement, oXMLTempAttr As XmlAttribute, oXMLTempNodes As XmlNodeList
		Dim sReturn As String = ""
		Try
			oXMLTempNodes = oXMLParentElement.GetElementsByTagName("FieldName")
			If oXMLTempNodes.Count > 0 Then
				oXMLTempNode = oXMLTempNodes(0)
				If TypeOf (oXMLTempNode) Is XmlElement Then
					sReturn = oXMLTempNode.InnerText

				Else
					sReturn = oXMLTempNode.Value

				End If
			End If
		Catch ex As Exception
			Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
		End Try
		Return sReturn
	End Function

	'Public Function GetHTTPResponse(sURLs As ArrayList, sXMLMessage As String) As String
	'    Dim sReturn As String = ""
	'    Dim oWebReq As WebRequest, oWebResponse As HttpWebResponse
	'    Dim aBytes() As Byte, sResponseText As String
	'    Dim oSubst As clsValueSubstitutions, oWriteStream As Stream
	'    Dim oReturnStream As Stream, oStreamReader As StreamReader
	'    Dim oCredBase As NetworkCredential, iStatusCode As HttpStatusCode

	'    Try


	'        aBytes = Encoding.ASCII.GetBytes(sXMLMessage)

	'        For Each sURL In sURLs
	'            oWebReq = WebRequest.Create(sURL)
	'            'oWebReq.Credentials = oCredBase
	'            oWebReq.Method = WebRequestMethods.Http.Post

	'            oWebReq.ContentType = "text/xml; charset=utf-8"

	'            oWriteStream = oWebReq.GetRequestStream()
	'            oWriteStream.Write(aBytes, 0, aBytes.Length)
	'            oWriteStream.Close()

	'            oWebResponse = DirectCast(oWebReq.GetResponse(), HttpWebResponse)
	'            oReturnStream = oWebResponse.GetResponseStream()
	'            oStreamReader = New StreamReader(oReturnStream)

	'            sResponseText = oStreamReader.ReadToEnd()
	'            iStatusCode = oWebResponse.StatusCode
	'            'sResponseCode = oWebResponse.  .StatusCode.tostring
	'            oWebResponse.Close()
	'            oWebResponse = Nothing
	'            oWebReq = Nothing

	'            If iStatusCode = HttpStatusCode.OK Or iStatusCode = HttpStatusCode.Accepted Then
	'                sReturn = sResponseText
	'                Exit For
	'            End If

	'        Next

	'    Catch ex As Exception
	'        Dim bDebugSetting As Boolean = False
	'        Try : bDebugSetting = StringIsTrue(GetMySettingString("InDevTest")) : Catch ex2 As Exception : End Try
	'        If bDebugSetting Then
	'            Dim oErrItem As ErrorItem
	'            oErrItem = glErrors.NewError(ex)
	'        End If

	'    End Try
	'    Return sReturn
	'End Function




	Public Function MakeStringFileNameSafe(NameString As String) As String
		Dim sReplacementChar As String = "", sChar As Char
		Dim sReturn As String = NameString, sInvalidChars As String
		Try
			sInvalidChars = IO.Path.GetInvalidFileNameChars

			For Each sChar In sInvalidChars
				sReturn = sReturn.Replace(sChar, sReplacementChar)
			Next

		Catch ex As Exception
			Dim oError As ErrorItem = glErrors.NewError(ex)

		End Try
		Return sReturn
	End Function



	Public Function MakeStringFilePathSafe(PathString As String) As String
		Dim sReplacementChar As String = "", sChar As Char
		Dim sReturn As String = PathString, sInvalidChars As String
		Dim iPos As Integer, sPath As String = "", sFileName As String = PathString
		Try
			iPos = PathString.LastIndexOf(IO.Path.DirectorySeparatorChar)
			If iPos > 1 Then
				sPath = sReturn.Substring(0, iPos)
				sFileName = sReturn.Substring(iPos + 1)
			End If

			sInvalidChars = IO.Path.GetInvalidFileNameChars
			For Each sChar In sInvalidChars
				sFileName = sFileName.Replace(sChar, sReplacementChar)
			Next

			sInvalidChars = IO.Path.GetInvalidPathChars
			For Each sChar In sInvalidChars
				sPath = sPath.Replace(sChar, sReplacementChar)
			Next

			sReturn = sPath & IIf(sPath <> "", IO.Path.DirectorySeparatorChar, "").ToString & sFileName

		Catch ex As Exception
			Dim oError As ErrorItem = glErrors.NewError(ex)

		End Try
		Return sReturn
	End Function



	Public Function MakeStringSQLValueSafe(NameString As String) As String
		Dim sCharKey As String, sReplacementChars As New SortedList(Of String, String)(System.StringComparison.InvariantCultureIgnoreCase) From {{"'", "''"}}
		Dim sReturn As String = NameString

		Try
			For Each sCharKey In sReplacementChars.Keys
				sReturn = sReturn.Replace(sCharKey, sReplacementChars.Item(sCharKey))
			Next

		Catch ex As Exception
			Dim oError As ErrorItem = glErrors.NewError(ex)

		End Try
		Return sReturn
	End Function



	Public Function MakeStringHTMLSafe(NameString As String) As String
		Dim sCharKey As String, sReplacementChars As New SortedList(Of String, String)(System.StringComparison.InvariantCultureIgnoreCase) From {{"'", "''"}}
		Dim sReturn As String = NameString

		Try
			sReturn = WebUtility.HtmlEncode(sReturn)

			For Each sCharKey In sReplacementChars.Keys
				sReturn = sReturn.Replace(sCharKey, sReplacementChars.Item(sCharKey))
			Next

		Catch ex As Exception
			Dim oError As ErrorItem = glErrors.NewError(ex)

		End Try
		Return sReturn
	End Function



	Public Function MakeStringXMLSafe(NameString As String) As String
		'Dim sCharKey As String, sReplacementChars As New SortedList(Of String, String)(System.StringComparison.InvariantCultureIgnoreCase)
		Dim sReturn As String = NameString

		Try
			sReturn = SecurityElement.Escape(sReturn)

			'For Each sCharKey In sReplacementChars.Keys
			'	sReturn = sReturn.Replace(sCharKey, sReplacementChars.Item(sCharKey))
			'Next

		Catch ex As Exception
			Dim oError As ErrorItem = glErrors.NewError(ex)

		End Try
		Return sReturn
	End Function



	Public Function MakeStringJavascriptValueSafe(NameString As String) As String
		'Dim sCharKey As String, sReplacementChars As New SortedList(Of String, String)(System.StringComparison.InvariantCultureIgnoreCase) From {{"\", "\\"}, {"'", "\'"}, {"""", "\"""}, {"\b", "\\b"}, {"\f", "\\f"}, {"\n", "\\n"}}
		Dim sReturn As String = NameString

		Try
			sReturn = HttpUtility.JavaScriptStringEncode(sReturn)

			'For Each sCharKey In sReplacementChars.Keys
			'	sReturn = sReturn.Replace(sCharKey, sReplacementChars.Item(sCharKey))
			'Next

		Catch ex As Exception
			Dim oError As ErrorItem = glErrors.NewError(ex)

		End Try
		Return sReturn
	End Function



	Public Function MakeStringURLEncoded(NameString As String) As String
		'Dim sCharKey As String, sReplacementChars As New SortedList(Of String, String)(System.StringComparison.InvariantCultureIgnoreCase) From {{"'", "''"}}
		Dim sReturn As String = NameString

		Try
			sReturn = Uri.EscapeDataString(sReturn)

			'For Each sCharKey In sReplacementChars.Keys
			'	sReturn = sReturn.Replace(sCharKey, sReplacementChars.Item(sCharKey))
			'Next

		Catch ex As Exception
			Dim oError As ErrorItem = glErrors.NewError(ex)

		End Try
		Return sReturn
	End Function



	Public Function MakeStringURLDecoded(URIString As String) As String
		'Dim sCharKey As String, sReplacementChars As New SortedList(Of String, String)(System.StringComparison.InvariantCultureIgnoreCase) From {{"'", "''"}}
		Dim sReturn As String = URIString

		Try
			sReturn = Uri.UnescapeDataString(sReturn)

			'For Each sCharKey In sReplacementChars.Keys
			'	sReturn = sReturn.Replace(sCharKey, sReplacementChars.Item(sCharKey))
			'Next

		Catch ex As Exception
			Dim oError As ErrorItem = glErrors.NewError(ex)

		End Try
		Return sReturn
	End Function



	Public Function EstEnd(iTotalRecs As Integer, iCurrentRec As Integer, dStartedAt As Date) As Date
		Dim dReturn As Date = Now, lTicks As Long

		If iTotalRecs > 0 And iCurrentRec > 0 Then
			lTicks = CLng(Math.Floor(((Now.Ticks - dStartedAt.Ticks) * iTotalRecs / iCurrentRec) + dStartedAt.Ticks))
			dReturn = New Date(lTicks)
		End If
		Return dReturn
	End Function


	'Public Function DurationToString(dDiff As Date) As String
	'    Dim sReturn As String = "", iDays As Integer
	'    Try

	'        iDays = dDiff.Hour \ 24
	'        sReturn = IIf(iDays > 0, iDays.ToString & " day" & IIf(iDays > 1, "s", "").ToString & " , ", "").ToString &
	'                        IIf(iDays > 0 Or dDiff.Hour > 0, (dDiff.Hour Mod 24).ToString & " hour" & IIf((dDiff.Hour Mod 24) > 1, "s", "") & " ", "").ToString &
	'                        ((dDiff.Minute Mod 60).ToString & " Minute" & IIf((dDiff.Minute Mod 60) > 1, "s", "").ToString & " ").ToString &
	'                        ((dDiff.Second Mod 60).ToString & " second" & IIf((dDiff.Second Mod 60) > 1, "s", "").ToString & " ")

	'    Catch ex As Exception
	'        Dim oError As ErrorItem = glErrors.NewError(ex)

	'    End Try
	'    Return sReturn
	'End Function


	'Public Sub SwapResource(sMainResource As String, sNewResourceURI As String)
	'    Dim oURI As Uri, oStreamInfo As StreamResourceInfo
	'    Dim oReader As System.Windows.Markup.XamlReader
	'    Dim myResourceDictionary As ResourceDictionary
	'    Dim iIndex As Integer, oRsrcDict As ResourceDictionary
	'    Try


	'        oURI = New Uri(sNewResourceURI, UriKind.Relative)
	'        oStreamInfo = Application.GetContentStream(oURI)
	'        oReader = New System.Windows.Markup.XamlReader()

	'        ' TODO: check out resource dictionary switching

	'        myResourceDictionary = TryCast(oReader.LoadAsync(oStreamInfo.Stream), ResourceDictionary)
	'        For iIndex = 0 To Application.Current.Resources.MergedDictionaries.Count
	'            oRsrcDict = Application.Current.Resources.MergedDictionaries.Item(iIndex)
	'            'If oRsrcDict. .Item(iIndex) = myResourceDictionary Then
	'        Next
	'        'Application.Current.Resources.MergedDictionaries.Add(myResourceDictionary)

	'    Catch ex As Exception

	'    End Try
	'End Sub

	Public Function SplitKeyPairsToDict(FullString As String, DelimUsed As Char) As Dictionary(Of String, String)
		Dim oReturn As New Dictionary(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
		Dim iMax As Integer, iPos As Integer, bInQuotes As Boolean = False
		Dim aParts As ArrayList, sTemp As String, sKey As String, sVal As String
		Dim sAccum As String = "", iCharPos As Integer, iCharPos2 As Integer

		Try

			bInQuotes = False
			aParts = New ArrayList(Split(FullString, DelimUsed))
			sAccum = ""
			iMax = aParts.Count - 1

			For iPos = iMax To 0 Step -1
				sTemp = aParts(iPos).ToString
				If Not bInQuotes And sTemp.LastIndexOf("""") = sTemp.Length Then bInQuotes = True
				If bInQuotes Then
					iCharPos = sTemp.IndexOf("""")
					iCharPos2 = sTemp.IndexOf("=")
					If iCharPos < sTemp.Length And iCharPos2 > 2 And iCharPos > iCharPos2 Then
						If sAccum <> "" Then sTemp = sTemp & ";" & sAccum
						sAccum = ""
						bInQuotes = False
					Else
						sAccum = sTemp & IIf(sAccum <> "", DelimUsed, "").ToString & sAccum
					End If
				End If
				If Not bInQuotes Then
					iCharPos = sTemp.IndexOf("=")
					If iCharPos > 0 Then
						sKey = sTemp.Substring(0, iCharPos)
						sVal = sTemp.Substring(iCharPos + 1)
					Else
						sKey = ""
						sVal = sTemp
					End If
					If oReturn.ContainsKey(sKey) Then oReturn.Item(sKey) = sVal Else oReturn.Add(sKey, sVal)
				End If
			Next
		Catch ex As Exception
			Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)

		End Try
		Return oReturn
	End Function



	Public Function SpaceOnCaseChange(ByVal sString As String) As String
		Dim sRegexEval As String = "([a-z])(([A-Z][a-z])|\d)", sReturn As String
		Dim sReplaceVal As String = "$1 $2"
		Try


			sReturn = Regex.Replace(sString, sRegexEval, sReplaceVal)

		Catch ex As Exception
			Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)

		End Try

		Return sReturn

	End Function


	'Public Function GetPrevControl(oControlIn As Object) As Control
	'    Dim oNewcontrol As Control, oControl As Object, oParentChildren As UIElementCollection
	'    Dim oAsPanel As Panel, oAsGrid As Grid, oParentPanel As Panel, oParentGrid As Grid, oParentObj As Object
	'    Dim iPos As Integer

	'    Try
	'        oControl = oControlIn
	'        Do
	'            oNewcontrol = TryCast(oControl, Control)
	'            oAsGrid = TryCast(oControl, Grid)
	'            oAsPanel = TryCast(oControl, Panel)
	'            oParentGrid = Nothing
	'            oParentPanel = Nothing
	'            oParentChildren = Nothing
	'            oParentObj = Nothing

	'            If oNewcontrol IsNot Nothing Then
	'                oParentObj = oNewcontrol.Parent

	'            ElseIf oAsGrid IsNot Nothing Then
	'                oParentObj = oAsGrid.Parent

	'            ElseIf oAsPanel IsNot Nothing Then
	'                oParentObj = oAsPanel.Parent

	'            End If
	'            If oParentObj IsNot Nothing Then
	'                oParentPanel = TryCast(oParentObj, Panel)
	'                oParentGrid = TryCast(oParentObj, Grid)
	'                If oParentPanel IsNot Nothing Then oParentChildren = oParentPanel.Children
	'                If oParentGrid IsNot Nothing Then oParentChildren = oParentGrid.Children

	'                If oParentChildren IsNot Nothing Then
	'                    iPos = oParentChildren.IndexOf(oControl)
	'                    If iPos > 0 Then
	'                        iPos -= 1
	'                        oControl = oParentChildren.Item(iPos)
	'                        oNewcontrol = TryCast(oControl, Control)
	'                    Else
	'                        oControl = oParentObj
	'                        oNewcontrol = Nothing
	'                    End If

	'                End If
	'            End If

	'        Loop Until oNewcontrol IsNot Nothing Or oControl Is Nothing Or oParentObj Is Nothing
	'    Catch ex As Exception
	'        Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)

	'    End Try
	'    Return oNewcontrol
	'End Function




	'Public Function GetNextControl(oControlIn As Object) As Control
	'    Dim oNewcontrol As Control, oControl As Object, oParentChildren As UIElementCollection, oChildChildren As UIElementCollection
	'    Dim oAsPanel As Panel, oAsGrid As Grid, oParentPanel As Panel, oParentGrid As Grid, oParentObj As Object
	'    Dim iPos As Integer

	'    Try
	'        oControl = oControlIn
	'        Do
	'            oNewcontrol = TryCast(oControl, Control)
	'            oAsGrid = TryCast(oControl, Grid)
	'            oAsPanel = TryCast(oControl, Panel)
	'            oParentGrid = Nothing
	'            oParentPanel = Nothing
	'            oParentChildren = Nothing
	'            oParentObj = Nothing

	'            If oNewcontrol IsNot Nothing Then
	'                oParentObj = oNewcontrol.Parent

	'            ElseIf oAsGrid IsNot Nothing Then
	'                'oParentObj = oAsGrid.Parent
	'                oParentObj = oAsGrid.Parent

	'            ElseIf oAsPanel IsNot Nothing Then
	'                'oParentObj = oAsPanel.Parent
	'                oParentObj = oAsPanel.Parent

	'            End If
	'            If oParentObj IsNot Nothing Then
	'                oParentPanel = TryCast(oParentObj, Panel)
	'                oParentGrid = TryCast(oParentObj, Grid)
	'                If oParentPanel IsNot Nothing Then oParentChildren = oParentPanel.Children
	'                If oParentGrid IsNot Nothing Then oParentChildren = oParentGrid.Children

	'                If oParentChildren IsNot Nothing Then
	'                    iPos = oParentChildren.IndexOf(oControl)
	'                    If iPos >= 0 And iPos < oParentChildren.Count - 1 Then
	'                        iPos += 1
	'                        oControl = oParentChildren.Item(iPos)
	'                        oAsGrid = TryCast(oControl, Grid)
	'                        oAsPanel = TryCast(oControl, Panel)
	'                        oChildChildren = Nothing
	'                        If oAsPanel IsNot Nothing AndAlso oAsPanel.Children.Count > 0 Then oChildChildren = oAsPanel.Children
	'                        If oChildChildren Is Nothing And oAsGrid IsNot Nothing AndAlso oAsGrid.Children.Count > 0 Then oChildChildren = oAsGrid.Children
	'                        If oChildChildren IsNot Nothing Then oControl = oChildChildren(0)
	'                        oNewcontrol = TryCast(oControl, Control)

	'                    Else
	'                        oControl = oParentObj
	'                        oNewcontrol = Nothing

	'                    End If
	'                End If

	'            End If

	'        Loop Until oNewcontrol IsNot Nothing Or oControl Is Nothing Or oParentObj Is Nothing
	'    Catch ex As Exception
	'        Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)

	'    End Try
	'    Return oNewcontrol
	'End Function



End Module
