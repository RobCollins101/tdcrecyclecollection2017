﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tandridge.gov.uk/TDCRefuseCollection2017/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class wsRefuseCollection
	Inherits System.Web.Services.WebService

	<WebMethod()>
	Public Function CollectionDetails(LLPGUPRN As String, DateFor As String) As clsRefuseCollectionRecord
		Dim oReturn As clsRefuseCollectionRecord, dDateFor As Date = Nothing

		Date.TryParse(DateFor, dDateFor)

		oReturn = New clsRefuseCollectionRecord(LLPGUPRN, dDateFor)


		Return oReturn
	End Function

End Class